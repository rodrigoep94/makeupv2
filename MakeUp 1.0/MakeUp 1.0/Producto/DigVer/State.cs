﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Producto.DigVer
{    //Patron state y se aplica el singleton para menejar una sola instancia de cada objeto
     //Clase abstracta padre para que hereden sus hijo el metodo DVState();

    public abstract class State
        {
            public abstract String DVState();
            public String tabla { get; set; }
            public String registro { get; set; }
        }
        //Clase que se instancia si no hay errores de digitos verificadores.
        public class Optimo : State
        {
            private static Optimo instance;
            private Optimo()
            {
            }
            //Patron singleton para asegurarse de que existe solo una instancia de este tipo de objeto.
            public static Optimo getInstance()
            {
                if (instance == null)
                {
                    instance = new Optimo();
                }

                return instance;


            }

            //Metodo que se hereda y sobreescribe para devolver el mensaje en el que dice como se encuentra la base de datos.
            public override String DVState()
            {


                return "La BD se encuentra optima";
            }

        }
        //Clase que se instancia si hay errores de digitos verificadores horizontales.
        public class ErrorRegistro : State
        {
            private static ErrorRegistro instance;
            private ErrorRegistro()
            {

            }
            //Patron singleton para asegurarse de que existe solo una instancia de este tipo de objeto.
            public static ErrorRegistro getInstance()
            {
                if (instance == null)
                {
                    instance = new ErrorRegistro();
                }

                return instance;


            }

            //Metodo que se hereda y sobreescribe para devolver el mensaje en el que dice que se encontro un error en el DVH.
            public override String DVState()
            {

                return "Fallo el DVH ";
            }
        }
        //Clase que se instancia si hay errores de digitos verificadores verticales.
        public class ErrorTabla : State
        {
            private static ErrorTabla instance;
            //Constructor.
            private ErrorTabla() { }
            //Patron singleton para asegurarse de que existe solo una instancia de este tipo de objeto.
            public static ErrorTabla getInstance()
            {
                if (instance == null)
                {
                    instance = new ErrorTabla();
                }

                return instance;


            }
            //Metodo que se hereda y sobreescribe para devolver el mensaje en el que dice que se encontro un error en el DVV.
            public override String DVState()
            {

                return "Fallo el DVV";
            }
        }
    }