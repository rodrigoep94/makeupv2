﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.SqlServer;

namespace Producto.Controlador
{
    public class Conexion
    {
        public static bool RealizarBackup(string nombre, ref string message)
        {
            try
            {
                bool isOk = false;
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
                {

                    string AppPath = System.AppDomain.CurrentDomain.BaseDirectory;
                    AppPath += @"\Backup\" + nombre;
                    string query = "BACKUP DATABASE [LPPA2]";
                    query += string.Format(" TO  DISK = N'{0}'", AppPath);
                    query += " WITH NOFORMAT, NOINIT,  ";
                    query += string.Format(" NAME = N'{0}', ", nombre);
                    query += "SKIP, NOREWIND, NOUNLOAD,  STATS = 10";


                    conn.Open();
                    SqlCommand command = new SqlCommand(query, conn);
                    command.ExecuteNonQuery();
                    conn.Close();
                    isOk = true;
                    return isOk;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }


        }
        //Metodo que realiza el restore de la base de datos. Recibe por parametros el nombre del 
        //archivo del .bak para realizar el restore y un string que se pasa por
        //referencia para asignar en caso de que haya un error el valor
        //del error de base de datos.
        public static bool RelizarRestore(string archivo, ref string message)
        {
            bool generarRestore = false;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["restore"].ToString());
            String nombreBase = "LPPA2";
            String ubicacionBAK = System.AppDomain.CurrentDomain.BaseDirectory + @"\Backup\";
            string query = "EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'" + nombreBase + "' ; ALTER DATABASE " + nombreBase + " SET  SINGLE_USER WITH ROLLBACK IMMEDIATE; RESTORE DATABASE " + nombreBase + " FROM  DISK = N'" + ubicacionBAK + archivo + "' WITH RESTRICTED_USER, FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 10;ALTER DATABASE " + nombreBase + " SET  MULTI_USER WITH NO_WAIT";
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = connection;
            cmd.Connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                generarRestore = true;
                return generarRestore;
            }
            catch (Exception ex)
            {

                message = ex.ToString();
            }
            finally
            {
                cmd.Connection.Close();

            }

            return generarRestore;

        }
    }
}