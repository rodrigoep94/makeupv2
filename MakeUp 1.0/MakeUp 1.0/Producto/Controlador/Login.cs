﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;

namespace Producto.Controlador
{
    public class Login
    {
        //Metodo que busca en la base de datos los permisos por tipos de usuarios y devuelve una lista de MenuItems
        //Para agregar al menu que se encuentra en la MasterPage.
        public static List<MenuItem> SetPermisos(int userType)
        {
            List<MenuItem> lista = new List<MenuItem>();
            //Obtengo el String de Conexion que se encuentra en el WebConfig.
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                //Obtengo los permisos en ADO.Net modo Conectado.

                conn.Open();
                string sql = "select menuxtipousuario.idtipousuario,menu.descripcion,menu.orden from menuxtipousuario,menu";
                sql += " where menuxtipousuario.idmenu = menu.id";
                sql += string.Format(" and idtipousuario = {0} order by menu.orden", userType);
                SqlCommand command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string nombre = reader.GetValue(1).ToString();
                        MenuItem menu = new MenuItem(nombre, nombre);
                        lista.Add(menu);

                    }

                }

                conn.Close();

            }

            return lista;
        }
        //Metodo estatico para no instanciar un objteo de la clases Login que devuelve si existe el usuario en la base de datos o no.
        public static bool existeUsuario(string usuario)
        {
            bool existe = false;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();
                string sql2 = string.Format("select count(*) from usuarios where usu_mail = '{0}'", usuario);
                SqlCommand command2 = new SqlCommand(sql2, conn);
                int count = Convert.ToInt32(command2.ExecuteScalar());
                if (count.Equals(1))
                {
                    existe = true;
                }
                conn.Close();
            }

            return existe;
        }
        //Metodo statico para no instanciar un objteo de la clase Login y que realiza el update a 0 del CII(Contador de ingresos incorrectos)
        public static void reiniciarContador(Dal.Usuario user)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();
                string sql2 = string.Format("update Usuarios set intentos = 0 where mail_Usuario = '{0}'", user.NombreUsuario);
                SqlCommand command2 = new SqlCommand(sql2, conn);
                command2.ExecuteNonQuery();
                //Luego de actulizar el contado actualizo el DVH y DVV.
                DigVer.DigVer.actualizarDVH("usuarios", user.id);
                conn.Close();
            }
        }
        //Metodo statico para no instanciar un objteo de la clase Login que aumenta el CII en la base de datos.
        public static bool AumentarContador(string usuario)
        {
            Bitacora bitacora = new Bitacora();
            bool estaBloqueado = false;

            string sql = string.Format("select id,usu_intentos from Usuarios where usu_mail = '{0}'", usuario);
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();

                SqlCommand command = new SqlCommand(sql, conn);
                int intentos = 0;
                int id = 0;
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        id = Convert.ToInt32(reader.GetValue(0));
                        intentos = Convert.ToInt32(reader.GetValue(1));
                    }
                }
                conn.Close();
                //Si la cantidad de intentos es 3 el usuario esta bloqueado.
                if (intentos.Equals(2))
                {
                    //Inserto en la bitacora el suceso.
                    bitacora.ErrorBloqueoUsuarioState(usuario);
                    estaBloqueado = true;
                    return estaBloqueado;
                }
                else
                {
                    conn.Open();
                    //Si el usuario no esta bloqueado todavia realizo el update.
                    string sql2 = string.Format("update Usuarios set usu_intentos = usu_intentos +1 where usu_mail = '{0}'", usuario);
                    SqlCommand command2 = new SqlCommand(sql2, conn);
                    command2.ExecuteNonQuery();
                    //Inserto en bitacora el suceso.
                    bitacora.ErrorFalloContraseñaState(usuario);
                    //ACtualizo el DVH y DVV.
                    DigVer.DigVer.actualizarDVH("Usuarios", id);
                }
                conn.Close();

            }
            return estaBloqueado;

        }
        //Metodo statico para no instanciar un objeto de la clase Login que autentica al usuario si existe en la base de datos
        //y devuelve un objeto usuario con todas sus propiedades.
        public static Dal.Usuario Autenticar(string usuario, string password)
        {

            string sql = @"SELECT id,usu_nombre,usu_apellido,usu_mail,TipoUsuario
                      FROM Usuarios
                      WHERE usu_mail = @nombre AND usu_pass = @password and usu_intentos != 2";
            //string sql = @"SELECT id,usu_nombre,usu_apellido,usu_mail
            //          FROM Usuarios
            //          WHERE usu_mail = @nombre AND usu_pass = @password and usu_intentos != 2";
            //inicializo el usuario en null antes de llenarlo de datos.
            Dal.Usuario user = null;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("@nombre", usuario);

                //Concateno el usuario y contraseña y luego lo encripto ya que al dar de alta el usuario se realizo de esta misma forma
                //y asi validar contra la base de datos. El resultado de encriptacion es el mismo que se encuentra en la base de datos.
                string hash = Encriptar.GetMD5(string.Concat(usuario, password));
                //la linea de abajo es la posta, las otras dos las agregue para superar lo de hash
                                command.Parameters.AddWithValue("@password", hash);
                //command.Parameters.AddWithValue("@password", usuario);

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //Instancio un objeto usuario y lo lleno de datos.
                        user = new Dal.Usuario();
                        user.id = Convert.ToInt32(reader.GetValue(0));
                        user.Nombre = reader.GetValue(1).ToString();
                        user.Apellido = reader.GetValue(2).ToString();
                        user.NombreUsuario = reader.GetValue(3).ToString();
                        user.TipoUsuario = Convert.ToInt32(reader.GetValue(4));


                    }

                }
                conn.Close();
                //En caso que el usuario sea nulo lo devuelo para que se valide la clase Login.aspx.
                return user;

            }
        }

    }
}