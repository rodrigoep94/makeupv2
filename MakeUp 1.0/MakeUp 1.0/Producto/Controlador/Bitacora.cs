﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Producto.Controlador
{
    //Clase que maneja la bitacora, y contiene el patron state para verificar el tipo
    //de registro que se va a insertar en bitacora. 
    public class Bitacora
    {
        private IState state;
        //Metodo que inserta en bitacora en caso de que haya un error de 
        //Digito verificador vertical. Se convierte la clase en un tipo de ErrorDigVV.
        //en la cual se pasa por parametro la tabla en la que ocurrio el evento.
        public void ErrorDVVState(string tableName)
        {
            state = new ErrorDigVV(tableName);
            state.insertBitacora();
        }
        //Metodo que inserta en bitacora en caso de que haya un error de 
        //Digito verificador Horizontal. Se convierte la clase en un tipo de ErrorDigVH.
        //en la cual se para por parametro la tabla y el registro en donde ocurrio el evento.
        public void ErrorDVHState(string tableName, int idReg)
        {
            state = new ErrorDigVH(tableName, idReg);
            state.insertBitacora();
        }
        //Metodo que inserta en bitacora en caso de que haya suceda un
        //fallo de contraseña. Se conviernte la clase a un tipo de FalloContraseña.
        //en la cual se pasa por parametro el usuario que produjo el evento.
        public void ErrorFalloContraseñaState(string Usuario)
        {
            state = new FalloContraseña(Usuario);
            state.insertBitacora();
        }
        //Metodo que inserta en bitacora en caso de que haya suceda un
        //bloqueo de usuario. Se conviernte la clase a un tipo de UsuarioBloqueado.
        //en la cual se pasa por parametro el usuario que produjo el evento.
        public void ErrorBloqueoUsuarioState(string Usuario)
        {
            state = new UsuarioBloqueado(Usuario);
            state.insertBitacora();
        }
        //Metodo que se conecta a la base de datos y devuelve todos los items de bitacora
        //que muestra los eventos mas cercanos primeros en la lista.
        public static DataTable obtenerBitacora()
        {
            string sql = @"select * from bitacora order by 1 desc";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Bitacora");
            return ds.Tables["Bitacora"];

        }
        //Metodo que utilizo para obtener los filtros de bitacora
        public static DataTable obtenerFiltro(string tabla, string filtro)
        {
            string sql = string.Format("select distinct({0}) from {1} order by 1 desc",filtro, tabla);
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Filtro");
            return ds.Tables["Filtro"];
        }
        //Metodo que utilizo para obtener el resultado de la busqueda en bitacora con los filtros, 
        //dependiendo lo que se envie es lo que devuelvo

        public static DataTable obtenerResultadoBusqueda(string filtro1, string filtro2, string filtro3, DateTime fechaDesde, DateTime fechaHasta)
        {
            //string sql = string.Format(@"select * from bitacora where bit_fecha='{0}' order by 1 desc", fechaDesde.ToString("MM/dd/yyyy HH:mm"));
            string sql = string.Format(@"select * from bitacora where bit_fecha >='{0}' and bit_fecha <='{1}'  order by 1 desc", fechaDesde.ToString("MM/dd/yyyy"), fechaHasta.ToString("MM/dd/yyyy"));

            if (filtro1 != "" && filtro2 !="" && filtro3 !=""  ) {
                sql = string.Format("select * from bitacora where bit_usu='{0}' and bit_criticidad='{1}' and bit_fun='{2}' and bit_fecha >='{3}' and bit_fecha <='{4}'  order by 1 desc", filtro1, filtro2, filtro3, fechaDesde.ToString("MM/dd/yyyy"), fechaHasta.ToString("MM/dd/yyyy"));               
            }
            else if (filtro1 != "" && filtro2 == "" && filtro3 == "")
            {
                sql = string.Format("select * from bitacora where bit_usu='{0}' and bit_fecha >='{1}' and bit_fecha <='{2}'  order by 1 desc", filtro1, fechaDesde.ToString("MM/dd/yyyy"), fechaHasta.ToString("MM/dd/yyyy"));
            }
            else if (filtro1 != "" && filtro2 != "" && filtro3 == "")
            {
                sql = string.Format("select * from bitacora where bit_usu='{0}' and bit_criticidad='{1}' and bit_fecha >='{2}' and bit_fecha <='{3}'  order by 1 desc", filtro1, filtro2, fechaDesde.ToString("MM/dd/yyyy"), fechaHasta.ToString("MM/dd/yyyy"));
            }
            else if (filtro1 == "" && filtro2 == "" && filtro3 != "")
            {
                sql = string.Format("select * from bitacora where and bit_fun='{0}' and bit_fecha >='{1}' and bit_fecha <='{2}'  order by 1 desc", filtro3, fechaDesde.ToString("MM/dd/yyyy"), fechaHasta.ToString("MM/dd/yyyy"));

            }
            else if (filtro1 == "" && filtro2 != "" && filtro3 == "")
            {
                sql = string.Format("select * from bitacora where bit_criticidad='{0}' and bit_fun='{1}' and bit_fecha >='{2}' and bit_fecha <='{3}'  order by 1 desc", filtro2, filtro3, fechaDesde.ToString("MM/dd/yyyy"), fechaHasta.ToString("MM/dd/yyyy"));
            }
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Filtro");
            return ds.Tables["Filtro"];
        }

    }
}