﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace Producto.Controlador
{
    //Interface para que implementen todas las clases de bitacora.
    interface IState
    {
        String DVState();
        void insertBitacora();
    }
    //Clase que se instancia cuando existe un error de Digito verificador horizontal.
    public class ErrorDigVH : IState
    {

        private string _tableName;
        private int _reg;
        private string _user;
        //Constructor de la clase que recibe por parametro la tabla y el registro donde ocurrio
        //el evento. Para luego poder insertar en bitacora.
        public ErrorDigVH(string tabla, int idReg)
        {
            this._tableName = tabla;
            this._reg = idReg;
            this._user = "Generico";
        }
        //Metodo que devuele el datoa insertar en el campo descripcion de la bitacora.
        public String DVState()
        {
            return string.Format("Error en DVH en tabla {0} registro {1}", _tableName, _reg.ToString());
        }
        //Metodo que se conecta a la base de datos e inserta el registro del evento.
        public void insertBitacora()
        {
            //string sql = @"INSERT INTO bitacora (
            //               Descripcion
            //              ,usuario
            //              ,fecha                          
            //              ,criticidad)
            //          VALUES (
            //                @desc, 
            //                @usuario, 
            //                getDate(),
            //                @criticidad)";

            string sql = @"INSERT INTO bitacora (
                           bit_usu,
                           bit_fun,
                           bit_fecha,
                           bit_criticidad                          
                          )
                      VALUES (
                             
                            @usuario,
                            @desc, 
                            getDate(),
                            @criticidad)";


            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {


                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("desc", this.DVState());
                command.Parameters.AddWithValue("usuario", _user);
                command.Parameters.AddWithValue("criticidad", "Alta");

                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();

            }
        }
    }
    //Clase que se instancia cuando existe un error de Digito verificador vertical.
    public class ErrorDigVV : IState
    {
        private string _tableName;
        private string _user;
        //Constructor de la clase que recibe por parametro la tabla y donde ocurrio
        //el evento. Para luego poder insertar en bitacora.
        public ErrorDigVV(string tabla)
        {
            this._tableName = tabla;
            this._user = "Generico";
        }
        //Metodo que devuele el datoa insertar en el campo descripcion de la bitacora.
        public String DVState()
        {
            return string.Format("Error en Digito Verificador Vertical en tabla {0} ", _tableName);
        }
        //Metodo que se conecta a la base de datos e inserta el registro del evento.
        public void insertBitacora()
        {
            //string sql = @"INSERT INTO bitacora (
            //               Descripcion
            //              ,usuario
            //              ,fecha                          
            //              ,criticidad)
            //          VALUES (
            //                @desc, 
            //                @usuario, 
            //                getDate(),
            //                @criticidad)";

            string sql = @"INSERT INTO bitacora (
                           bit_usu,
                           bit_fun,
                           bit_fecha,
                           bit_criticidad                          
                          )
                      VALUES (
                             
                            @usuario,
                            @desc, 
                            getDate(),
                            @criticidad)";


            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                DateTime dtCurrTime = DateTime.Today;
                string d = dtCurrTime.ToString();

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("desc", this.DVState());
                command.Parameters.AddWithValue("usuario", _user);
                command.Parameters.AddWithValue("criticidad", "Alta");

                //string password = Helper.EncodePassword(string.Concat(usuario.NombreUsuario, usuario.Password));

                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();

            }

        }
    }
    //Clase que se instancia cuando un usuario no ingresa correctamente la contraseña.
    public class FalloContraseña : IState
    {
        private string _user;
        //Constructor de la clase que recibe por parametro el usuario que produjo
        //el evento. Para luego poder insertar en bitacora.
        public FalloContraseña(string Usuario)
        {
            this._user = Usuario;
        }
        //Metodo que devuele el datoa insertar en el campo descripcion de la bitacora.
        public String DVState()
        {
            return "Error en ingresar Contraseña";
        }
        //Metodo que se conecta a la base de datos e inserta el registro del evento.
        public void insertBitacora()
        {
            //string sql = @"INSERT INTO bitacora (
            //               Descripcion
            //              ,usuario
            //              ,fecha                          
            //              ,criticidad)
            //          VALUES (
            //                @desc, 
            //                @usuario, 
            //                getDate(),
            //                @criticidad)";

            string sql = @"INSERT INTO bitacora (
                           bit_usu,
                           bit_fun,
                           bit_fecha,
                           bit_criticidad                          
                          )
                      VALUES (
                             
                            @usuario,
                            @desc, 
                            getDate(),
                            @criticidad)";



            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                DateTime dtCurrTime = DateTime.Today;
                string d = dtCurrTime.ToString();

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("desc", this.DVState());
                command.Parameters.AddWithValue("usuario", _user);
                command.Parameters.AddWithValue("criticidad", "Baja");

                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();

            }
        }
    }
    //Clase que se instancia cuando un usuario es bloqueado.
    public class UsuarioBloqueado : IState
    {

        private string _user;
        //Constructor de la clase que recibe por parametro el usuario que produjo
        //el evento. Para luego poder insertar en bitacora.
        public UsuarioBloqueado(string Usuario)
        {
            this._user = Usuario;
        }
        //Metodo que devuele el datoa insertar en el campo descripcion de la bitacora.
        public String DVState()
        {
            return "Se bloqueo el usuario";
        }
        //Metodo que se conecta a la base de datos e inserta el registro del evento.
        public void insertBitacora()
        {
            //string sql = @"INSERT INTO bitacora (
            //               Descripcion
            //              ,usuario
            //              ,fecha                          
            //              ,criticidad)
            //          VALUES (
            //                @desc, 
            //                @usuario, 
            //                getDate(),
            //                @criticidad)";

            string sql = @"INSERT INTO bitacora (
                           bit_usu,
                           bit_fun,
                           bit_fecha,
                           bit_criticidad                          
                          )
                      VALUES (
                             
                            @usuario,
                            @desc, 
                            getDate(),
                            @criticidad)";


            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                DateTime dtCurrTime = DateTime.Today;
                string d = dtCurrTime.ToString();

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("desc", this.DVState());
                command.Parameters.AddWithValue("usuario", _user);
                command.Parameters.AddWithValue("criticidad", "Media");

                //string password = Helper.EncodePassword(string.Concat(usuario.NombreUsuario, usuario.Password));

                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();

            }
        }
    }
}