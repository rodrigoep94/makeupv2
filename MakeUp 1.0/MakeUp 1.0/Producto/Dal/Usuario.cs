﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Producto.Dal
{
    public class Usuario
    {
        //Propiedades del usuario
        public int id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public string NombreUsuario { get; set; }
        public string Password { get; set; }
        public int TipoUsuario { get; set; }

        //Metodo para realizar altas de usuario
        public static Usuario Insert(Usuario usuario)
        {

            string sql = @"INSERT INTO Usuarios2 (
                           Nombre
                          ,Apellido
                          ,Usuario
                          ,Password
                          ,TipoUsuario
                          ,intentos
                          ,DV)
                      VALUES (
                            @Nombre, 
                            @Apellido, 
                            @Usuario,
                            @Password,
                            @TipoUsuario,0,0)
                    SELECT SCOPE_IDENTITY()";

            //Obtengo el string de conexion que se encuentra en el WebConfig
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("Nombre", usuario.Nombre);
                command.Parameters.AddWithValue("Apellido", usuario.Apellido);
                command.Parameters.AddWithValue("Usuario", usuario.NombreUsuario);
                command.Parameters.AddWithValue("TipoUsuario", usuario.TipoUsuario);

                //Concateno el usuario y contraseña para genera un nivel de seguridad mas alto y lo encripto 
                //con el algoritmo MDG para guardarlo en la Base de datos.
                string password = Encriptacion.GetMD5(string.Concat(usuario.NombreUsuario, usuario.Password));
                command.Parameters.AddWithValue("Password", password);

                conn.Open();

                //Obtengo el id del usuario luego de insertarlo para actualizar el DVH.
                usuario.id = Convert.ToInt32(command.ExecuteScalar());
                //Actualizo el DVH y DVV.
                DigVer.DigVer.actualizarDVH("Usuarios2", usuario.id);
                //Cierro la conexion.
                conn.Close();
                //Devuelo el usuario creado con el id.
                return usuario;

            }
        }
        //Obtiene los tipos de usuarios existentes. (Webmaste,Administrador,Cliente).
        public static DataTable GetUserTypes()
        {

            string sql = @"select * from tipousuarios";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "UserTypes");
            return ds.Tables["UserTypes"];
        }

    }
}