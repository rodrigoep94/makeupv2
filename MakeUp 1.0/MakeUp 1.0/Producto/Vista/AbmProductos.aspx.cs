﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using CNegocio;

namespace Producto.Vista
{
    public partial class AbmProductos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Label1.Visible = true;

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (this.ValidateForm())
            {
                var bitacora = new CNBitacora();

                string nombre = txtNombre.Text;
                int precio = Convert.ToInt32(txtPrecio.Text);
                int cant = Convert.ToInt32(txtCantidad.Text);
                int stock = Convert.ToInt32(txtStock.Text);
                string ruta = Server.MapPath("~/Productos/" + FileUpload1.FileName);
                string rutaToInsert = "~/Productos/Prod_" + FileUpload1.FileName;

                CEntidades.CEProducto p = new CEntidades.CEProducto();
                p.Descripcion = nombre;
                p.precio = precio;
                p.cantidad = cant;
                p.urlImagen = rutaToInsert;
                p.stock = stock;

                var cnProducto = new CNProductos();
                p = cnProducto.insertarProducto(p);

                if (p.id != 0)
                {
                    //FileUpload1.SaveAs(ruta);
                    string ruta2 = Server.MapPath("~/Productos/Prod_" + FileUpload1.FileName);
                    System.IO.File.WriteAllBytes(ruta2, ResizeImageFile(FileUpload1.FileBytes, 25));
                    this.Label1.Text = "El producto fue dado de alta con éxito!";
                    this.Label1.Visible = true;
                    this.ResetForm();
                    var usuario = (CEntidades.CEUsuario)Session["User"];
                    bitacora.GuardarBitacora($"El producto {p.Descripcion} ha sido dado de alta con éxito", usuario.NombreUsuario);
                }
                else
                {
                    this.Label1.Text = "Error al registrar imagen en el servidor";
                    this.Label1.Visible = true;
                    bitacora.GuardarBitacora("Hubo un error al intentar guadar un producto", Session["User"].ToString());
                }
            }

        }

        // Para dimensionar la imagen podria hacer algo como esto.

        protected bool ValidateForm()
        {
            var listLabels = new List<Label>()
            {
                lblNombreValidator,
                lblPrecioValidator,
                lblCantidadValidator,
                lblStockValidator
            };

            var listForms = new List<TextBox>()
            {
                txtNombre,
                txtPrecio,
                txtCantidad,
                txtStock
            };

            var valid = true;

            for (int i = 0; i < listLabels.Count(); i++)
            {
                if (listForms.ElementAt(i).Text == string.Empty)
                {
                    listLabels.ElementAt(i).Visible = true;
                    valid = valid && false;
                }
                else
                {
                    listLabels.ElementAt(i).Visible = false;
                }
            }

            if (!FileUpload1.HasFile)
            {
                lblImagenValidator.Visible = true;
                valid = false;
            }
            else
            {
                lblImagenValidator.Visible = false;
            }

            return valid;

        }

        private byte[] ResizeImageFile(byte[] imageFile, int targetSize)
        {
            using (System.Drawing.Image oldImage = System.Drawing.Image.FromStream(new MemoryStream(imageFile)))
            {
                Size newSize = CalculateDimensions(oldImage.Size, targetSize);
                using (Bitmap newImage = new Bitmap(newSize.Width, newSize.Height, PixelFormat.Format24bppRgb))
                {
                    using (Graphics canvas = Graphics.FromImage(newImage))
                    {
                        canvas.SmoothingMode = SmoothingMode.AntiAlias;
                        canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        canvas.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        canvas.DrawImage(oldImage, new Rectangle(new Point(0, 0), newSize));
                        MemoryStream m = new MemoryStream();
                        newImage.Save(m, ImageFormat.Jpeg);
                        return m.GetBuffer();
                    }
                }
            }
        }

        private void ResetForm()
        {
            txtCantidad.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtPrecio.Text = string.Empty;
            txtStock.Text = string.Empty;
        }

        // Para determinar las dimensiones de la imagen podria hacer algo como esto.

        private Size CalculateDimensions(Size oldSize, int targetSize)
        {
            Size newSize = new Size();

            newSize.Height = 92;
            newSize.Width = 65;


            return newSize;
        }
    }
}