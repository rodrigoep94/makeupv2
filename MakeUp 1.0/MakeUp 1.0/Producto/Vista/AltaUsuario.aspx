﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AltaUsuario.aspx.cs" Inherits="Producto.Vista.AltaUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 126px;
        }
    </style>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="login">

    <table>
    
        <tr>
            <td class="style1">
                <p class="newUser">Nombre:</p>
            </td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    
            </td>
        </tr>
        <tr>
            <td class="style1">
                <p class="newUser">Apellido:</p>
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <p class="newUser">Usuario:</p>
            </td>
            <td>
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <p class="newUser">Contraseña:</p>
            </td>
            <td>
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <p class="newUser">TipoUsuario:</p>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" Height="18px" Width="133px">
                    <asp:ListItem Value="1">WebMaster</asp:ListItem>
                    <asp:ListItem Value="2">Administrador</asp:ListItem>
                    <asp:ListItem Value="3">Cliente</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style1">
               
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Crear Usuario" Width="128px" 
                    onclick="Button1_Click" />
            </td>
            
        </tr>
                
    </table>
    <asp:Label ID="Label1" runat="server" 
                    Text="El usuario se creó de forma exitosa" ForeColor="White" Visible="False"></asp:Label>
                
</div>

</asp:Content>

