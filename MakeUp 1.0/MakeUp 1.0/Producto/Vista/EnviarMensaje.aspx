﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="EnviarMensaje.aspx.cs" Inherits="Producto.Vista.EnviarMensaje" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3 class="titulos"><asp:Label runat="server" ID="Label2" Text="<%$Resources:Global, sendMessage %>"></asp:Label></h3>
    <br />
    <br />
    <table>
        <tr>
            <td class="style1">
                <asp:label id="lblNombre" runat="server" Text="<%$Resources:Global, sender %>" forecolor="White">
                <asp:Label Text="*" runat="server" style="color:red; font-weight:bold" />
                </asp:label>
            </td>
            <td>
                <asp:textbox id="txtNombre" runat="server"></asp:textbox>
                <asp:label text="El nombre de emisor es requerido" id="lblNombreValidator" visible="false" runat="server" style="color: red; font-weight: bold" />
            </td>
        </tr>

        <tr>
            <td class="style1">
                <asp:label id="lblDestinatario" runat="server" Text="<%$Resources:Global, receiver %>" forecolor="White">
                <asp:Label Text="*" runat="server" style="color:red; font-weight:bold"/>
                </asp:label>
            </td>
            <td>
                <asp:DropDownList ID="ddlDestinatario" runat="server" >
                    <asp:ListItem Value="0" Text="<%$Resources:Global, selectUser %>"></asp:ListItem>
                </asp:DropDownList>
                <asp:label text="El destinatario es requerido" id="lblDestinatarioValidator" visible="false" runat="server" style="color: red; font-weight: bold" />
            </td>
        </tr>
        <tr>
            <td class="style1">
                <asp:label id="lblMensaje" runat="server" Text="<%$Resources:Global, message %>" forecolor="White">
                <asp:Label Text="*" runat="server" style="color:red; font-weight:bold"/>
                </asp:label>
            </td>
            <td>
                <asp:textbox id="txtMensaje" runat="server" textmode="multiline" columns="50" rows="5"></asp:textbox>
                <asp:label text="El mensaje es requerido" id="lblMensajeValidator" visible="false" runat="server" style="color: red; font-weight: bold" />
            </td>
        </tr>


        <tr>
            <td></td>
            <td>
                <asp:button id="btnEnviar" runat="server" Text="<%$Resources:Global, sendMessage %>"
                    onclick="btnEnviar_Click" />
            </td>
        </tr>
    </table>
    <asp:label id="Label1" runat="server" text="" forecolor="White" visible="false"></asp:label>
</asp:Content>
