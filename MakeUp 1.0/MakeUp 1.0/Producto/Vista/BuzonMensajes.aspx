﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="BuzonMensajes.aspx.cs" Inherits="Producto.Vista.BuzonMensajes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">   
    <h1 class="titulos">Mensajes recibidos</h1>

    <asp:GridView ID="tableMensajes"
        runat="server"
        AutoGenerateColumns="False"
        BackColor="White"
        BorderColor="#3366CC"
        ShowHeaderWhenEmpty="true"
        EmptyDataText="No hay mensajes para mostrar en este momento"
        BorderStyle="None"
        BorderWidth="1px"
        Style="width: 100%;"
        CellPadding="4">
        <RowStyle BackColor="White" ForeColor="#003399" />

        <Columns>
            <asp:TemplateField HeaderText="">
                <ItemTemplate>
                    <asp:LinkButton ID="btnLeer" runat="server" CommandArgument='<%# Bind("id_mensaje")%>' OnClick="EliminarId">
                        ✘
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Emisor">
                <ItemTemplate>
                    <asp:Label ID="LabelEmisor" runat="server" Text='<%# Bind("msj_emisor") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha Envio">
                <ItemTemplate>
                    <asp:Label ID="LabelFecha" runat="server" Text='<%# Bind("msj_fecha") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mensaje">
                <ItemTemplate>
                    <asp:Label ID="LabelMensaje" runat="server" Text='<%# Bind("msj_texto") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
    </asp:GridView>
</asp:Content>