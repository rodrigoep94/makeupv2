﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Remito.aspx.cs" Inherits="Producto.Vista.Remito" %>
<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1
        {
            width: 67px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="login">
        
        <table>
            <tr>
                <td class="style1">
                    <asp:Label ID="lblNombre" runat="server" Text="Nombre :" ForeColor="White"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                </td>
            </tr>
        
            <tr>
                <td class="style1">
                    <asp:Label ID="lblPrecio" runat="server" Text="Precio :" ForeColor="White"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPrecio" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="lblCantidad" runat="server" Text="Cantidad :" ForeColor="White"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCantidad" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="lblImg" runat="server" Text="Imagen :" ForeColor="White"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" ForeColor="White" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnUpload" runat="server" Text="Alta Producto" 
                        onclick="btnUpload_Click" />
                </td>
            </tr>
        </table>
        
        
       

    </div>
    <div class="UploadImg">
    
      
        <asp:Label ID="Label1" runat="server" Text="" ForeColor="White" Visible="false"></asp:Label>
    
    </div>
</asp:Content>
