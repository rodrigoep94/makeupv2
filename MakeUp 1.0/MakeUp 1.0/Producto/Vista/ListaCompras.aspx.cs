﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CEntidades;

namespace Producto.Vista
{
    public partial class ListaCompras : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Populate the GridView. 
                BindGridView();
            }
        }

        private void BindGridView()
        {
            var user = Session["User"] as CEntidades.CEUsuario;
            var cnVentas = new CNegocio.CNVentas();
            DataSet ds = cnVentas.ObtenerVentas(user.id);

            // Get the DataView from Person DataTable. 
            DataView dvVentas = ds.Tables["Ventas"].DefaultView;

            // Bind the GridView control. 
            tableVentas.DataSource = dvVentas;
            tableVentas.DataBind();
        }
    }
}