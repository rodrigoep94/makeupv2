﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Producto.Vista
//Pagina que se encarga de interactua contra la capa de acceso a datos
//y mostrar todos los registros de la tabla de bitacora en la pagina.
{
    public partial class Bitacora : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack) { 
                cargaDatos();
                cargaComboUsuarios();
                cargaComboCriticidad();
                cargaComboDescripcion();
            }
        }
        //Metodo que busca todos los registros en base de datos y los asigna a un
        //datagrid en la pagina.
        public void cargaComboUsuarios()
        {
            DataTable table = new DataTable();
            var cnBitacora = new CNegocio.CNBitacora();
            table = cnBitacora.obtenerFiltro("bitacora", "bit_usu");
            this.DDLUsuario.DataSource = table;
            this.DDLUsuario.DataTextField = "bit_usu";                            
            this.DDLUsuario.DataValueField = "bit_usu";
            this.DDLUsuario.DataBind();
            this.DDLUsuario.Items.Insert(0,"Seleccione un usuario");
            this.DDLUsuario.SelectedIndex = 0;
        }
        public void cargaComboCriticidad()
        {
            DataTable table = new DataTable();
            var cnBitacora = new CNegocio.CNBitacora();
            table = cnBitacora.obtenerFiltro("bitacora", "bit_criticidad");
            this.DDLCriticidad.DataSource = table;
            this.DDLCriticidad.DataTextField = "bit_criticidad";
            this.DDLCriticidad.DataValueField = "bit_criticidad";
            this.DDLCriticidad.DataBind();
            this.DDLCriticidad.Items.Insert(0, "Seleccione una criticidad");
            this.DDLCriticidad.SelectedIndex = 0;

        }
        public void cargaComboDescripcion()
        {
            DataTable table = new DataTable();
            var cnBitacora = new CNegocio.CNBitacora();
            table = cnBitacora.obtenerFiltro("bitacora", "bit_fun");
            this.DDLDescripcion.DataSource = table;
            this.DDLDescripcion.DataTextField = "bit_fun";
            this.DDLDescripcion.DataValueField = "bit_fun";
            this.DDLDescripcion.DataBind();
            this.DDLDescripcion.Items.Insert(0, "Seleccione una descripción");
            this.DDLDescripcion.SelectedIndex = 0;
        }

        public void cargaDatos()
        {

            DataTable table = new DataTable();
            table = CNegocio.CNBitacora.obtenerBitacora();
            this.GridView1.DataSource = table;
            this.GridView1.AllowPaging = true;
            this.GridView1.PageSize = 6;

            this.GridView1.DataBind();
        }
        //Evento que cambia de pagina en el datagrid y carga los datos.
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            cargaDatos();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            var cnBitacora = new CNegocio.CNBitacora();

            var fechaDesde = hiddenDateDesde.Value == "" ? new DateTime(2000, 01, 01).ToString("yyyyMMdd") : this.GetFormatedDateString(hiddenDateDesde.Value);
            var fechaHasta = hiddenDateHasta.Value == "" ? DateTime.Today.AddDays(1).ToString("yyyyMMdd") : this.GetFormatedDateString(hiddenDateHasta.Value);
            var usuario = DDLUsuario.SelectedIndex == 0 ? string.Empty : DDLUsuario.SelectedValue;
            var criticidad = DDLCriticidad.SelectedIndex == 0 ? string.Empty : DDLCriticidad.SelectedValue;
            var descripcion = DDLDescripcion.SelectedIndex == 0 ? string.Empty : DDLDescripcion.SelectedValue;

            table = cnBitacora.obtenerResultadoBusqueda(usuario, criticidad, descripcion, fechaDesde, fechaHasta);
            this.GridView1.DataSource = table;
            this.GridView1.AllowPaging = true;

            this.GridView1.DataBind();

        }

        private string GetFormatedDateString(string date)
        {
            var arrDate = date.Split('/');
            return $"{arrDate[2]}{arrDate[1]}{arrDate[0]}";
        }
    }
}