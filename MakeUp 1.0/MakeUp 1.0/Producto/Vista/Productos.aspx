﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="Producto.Vista.Productos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <fieldset class="fieldsetclass">
        <legend><asp:Label runat="server" ID="Label5" Text="<%$Resources:Global, productCatalog %>"></asp:Label></legend>
        <div id="divProductos" class="divProductos" runat="server">
            <asp:Repeater ID="rptProducts" runat="server" OnItemCommand="rptProducts_ItemCommand">
                <HeaderTemplate>
                    <table class="tableProducts">
                        <tr>
                            <th class="thProduct"><asp:Label runat="server" ID="Label4" Text="<%$Resources:Global, name %>"></asp:Label></th>
                            <th class="thProduct"><asp:Label runat="server" ID="Label3" Text="<%$Resources:Global, image %>"></asp:Label></th>
                            <th class="thProduct"><asp:Label runat="server" ID="Label2" Text="<%$Resources:Global, sellPrice %>"></asp:Label></th>
                            <th class="thProduct"><asp:Label runat="server" ID="Label1" Text="<%$Resources:Global, minimumQuantity %>"></asp:Label></th>
                            <th class="thProduct">Stock</th>
                            <th class="thProduct"></th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="trProduct">
                        <td class="tdPoduct"><%# Eval("nombre")%>  </td>
                        <td class="tdPoduct">
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("Imagen")%>' /></td>
                        <td class="tdPoduct">$ <%# Eval("precio")%></td>
                        <td class="tdPoduct"><%# Eval("cantidadMinima")%></td>
                        <td class="tdPoduct"><%# Eval("stock")%></td>
                        <td class="tdPoduct">
                            <asp:Button ID="btnAddToCart" CommandArgument='<%# Eval("id")%>' runat="server" Text="<%$Resources:Global, addToCart %>" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>




        </div>
    </fieldset>


    <br />
    <asp:Label ID="cantCompras" runat="server" ForeColor="White"></asp:Label>
    <br />
    <asp:HyperLink ID="HyperLink1" runat="server" ForeColor="Azure" NavigateUrl="~/Vista/Compra.aspx"><asp:Label runat="server" ID="Label4" Text="<%$Resources:Global, selectedProducts %>"></asp:Label></asp:HyperLink>
</asp:Content>
