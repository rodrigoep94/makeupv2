﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Producto.Vista
{
    //Pagina que le muestra al webmaster el tipo de error de digito verificador
    //y hereda de la master page. Por lo tanto puede navegar por la pagina.
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Label2.Text = Request.QueryString["message"];
            this.Label2.ForeColor = System.Drawing.Color.White;
        }
    }
}