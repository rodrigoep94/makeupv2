﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CNegocio;

namespace Producto.Vista
{
    //Pagina que muestra la compra final y el importe total de la compra
    //al cliente. Y lo guarda en la base de datos la venta realizada.
    public partial class CompraListaFinal : System.Web.UI.Page
    {
        //En el load se muestran todos los productos y se asignan a un asp:Repeater para
        //mostrarlos por pantalla.
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Label1.Visible = false;
            this.Label1.Text = "La compra se realizó correctamente";

            CNCarritoCompra compra = Session["Seleccion"] as CNCarritoCompra;

            var productos = compra.obtenerProductos;
            productos.ForEach(p =>
            {
                p.subtotal = p.cantidad * p.precio;
            });

            rptCompra.DataSource = productos;
            rptCompra.DataBind();


        }
        //Muestro el total del importe de todos los productos en un textbox.
        protected void rptCompra_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            CNCarritoCompra compra = Session["Seleccion"] as CNCarritoCompra;
            TextBox text = e.Item.FindControl("txtTotal") as TextBox;
            if (text != null)
            {
                text.Text = compra.obtenerImporte().ToString();
            }
        }
        //Inserto la venta en la base de datos.
        protected void rptCompra_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            //Pagina que se encarga de mostrar los items en el carrito de la compra
            //para que el usuario pueda ver las compra temporal anterior a la compra final
            CNCarritoCompra compraFinal = Session["Seleccion"] as CNCarritoCompra;
            CEntidades.CEUsuario usuario = Session["User"] as CEntidades.CEUsuario;
            var cnProductos = new CNProductos();
            cnProductos.insertarVenta(usuario, compraFinal);
            Button btn = e.Item.FindControl("Button1") as Button;
            Session["Seleccion"] = new CNCarritoCompra();
            if (btn != null)
            {
                btn.Enabled = false;
            }
            this.Label1.Visible = true;

        }

    }
}