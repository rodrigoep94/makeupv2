﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using CNegocio;
namespace Producto.Vista
{
    public partial class Restore : System.Web.UI.Page
    {
        //Pagina que sirve para realizar el restore e interactuar 
        //Contra la capa de acceso a datos y realizar el restore.
        protected void Page_Load(object sender, EventArgs e)
        {
            //Utilizo un label para mostrar el estado en que se realizo
            //el restore. Por default lo hago invisible para mostrarlo
            //una vez finalizado el restore.
            Label2.Visible = false;
        }

        //Boton que realiza el evento de generar restore. Utilizo un Fileupload
        //para seleccionar el .bak para realizar lar operacion.
        protected void Button1_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string ruta = FileUpload1.FileName;

                string errorMessage = "";
                var cnDatabase = new CNegocio.CNDatabase();
                bool isOk = cnDatabase.RelizarRestore(ruta, ref errorMessage);
                //Si el restore finalizó sin inconvenientes muestro el estado
                //en un label.
                if (isOk)
                {

                    Label2.Text = "El restore finalizó correctamente";
                    Label2.Visible = true;
                }
                else
                {
                    Label2.Text = "Se produjo un error al realizar el restore :" + errorMessage;
                    Label2.Visible = true;
                }
            }
        }
    }
}