﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Resources;
using CNegocio;
namespace Producto.Vista
{
    public partial class Productos : System.Web.UI.Page
    {
        //Pagina que muestra el catalogo de productos. Si el usuario esta 
        //Logueado a la aplicacion lo deja realizar transacciones.
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Logged"].ToString() == "Yes")
            {
                //Obtengo los items seleccionados y muestro 
                //la cantidad  en un label.
                CNegocio.CNCarritoCompra carrito = Session["Seleccion"] as CNegocio.CNCarritoCompra;
                if (carrito != null && carrito.obtenerProductos.Count() > 0)
                {
                    this.cantCompras.Text = Resources.Global.itemsInCart + ": " + carrito.CantidadDeProductos.ToString();
                    this.cantCompras.Visible = true;
                    this.HyperLink1.Visible = true;
                }
                else
                {
                    this.HyperLink1.Visible = false;
                }

                if (carrito.obtenerProductos == null)
                {
                    this.HyperLink1.Visible = false;
                }
                else
                {
                    if (carrito.obtenerProductos.Count() == 0)
                    {
                        this.HyperLink1.Visible = false;
                    }
                }

            }
            else
            {
                //Si no esta loguedo no le muestro el hyperlink para ir a 
                //ver los productos que se encuentran en la pagina.
                this.HyperLink1.Visible = false;
                this.cantCompras.Visible = false;
            }

            //Si no es postBack agrego los items para que no se repiten
            //luego en la lista.
            if (!Page.IsPostBack)
            {

                //Muesto los productos existentes en la base de datos
                //para que el usuario pueda realizar la compra y seleccionar
                //del catalogo.
                List<CEntidades.CEItemProducto> productos = new List<CEntidades.CEItemProducto>();
                var cnProductos = new CNegocio.CNProductos();
                DataSet ds = cnProductos.obtenerProductos();
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    int id = Convert.ToInt32(item["id"]);
                    string desc = item["pro_Nombre"].ToString();
                    int precio = Convert.ToInt32(item["pro_Precio"]);
                    string imagen = item["pro_UrlImagen"].ToString();
                    int stock = item["pro_stockMinimo"] == DBNull.Value ? 0 : Convert.ToInt32(item["pro_cantidad"]);
                    int cantidadMinima = Convert.ToInt32(item["pro_stockMinimo"]);

                    productos.Add(new CEntidades.CEItemProducto(id, desc, precio, imagen, cantidadMinima, stock));


                }

                rptProducts.DataSource = productos;
                rptProducts.DataBind();
                //Guardo todos los productos en una variable de sesion.
                Session["Productos"] = productos;

            }

        }


        protected void rptProducts_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (Session["Logged"].Equals("Yes"))
            {
                int idProd = Convert.ToInt32(e.CommandArgument);

                List<CEntidades.CEItemProducto> productos = Session["Productos"] as List<CEntidades.CEItemProducto>;

                CEntidades.CEItemProducto prod = productos.Find(p => p.id == idProd);


                CNegocio.CNCarritoCompra carrito = Session["Seleccion"] as CNegocio.CNCarritoCompra;

                if (carrito == null) carrito = new CNCarritoCompra();

                var productosBusiness = new CNProductos();

                if (productosBusiness.HayStockDisponible(idProd))
                {
                    if (!carrito.existeProducto(prod))
                    {
                        carrito.agregarProducto(prod);
                        Session["Seleccion"] = carrito;
                        this.cantCompras.Visible = true;
                        this.cantCompras.Text = "Cantidad de objetos en el carrito : " + carrito.CantidadDeProductos.ToString();
                        this.HyperLink1.Visible = true;
                    }
                    else
                    {
                        this.cantCompras.Visible = true;
                        this.cantCompras.Text = "Ese producto ya se encuentra en el carrito de compra";
                    }
                }
                else
                {
                    this.cantCompras.Visible = true;
                    this.cantCompras.Text = "No hay stock minimo disponible del producto seleccionado.";
                    productosBusiness.NotificarFaltaStock(idProd);
                    Site1 MasterP = (Site1)this.Master;
                    MasterP.UpdateMenu();
                }

            }
            else
            {
                this.cantCompras.Text = "Debe inicar sesion para poder realizar una compra";
                this.cantCompras.Visible = true;

            }


        }
    }
}