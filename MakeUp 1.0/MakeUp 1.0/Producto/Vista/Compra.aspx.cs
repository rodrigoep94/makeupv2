﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CNegocio;
namespace Producto.Vista
{
    //Pagina que se encarga de mostrar los items en el carrito de la compra
    //para que el usuario pueda ver las compra temporal anterior a la compra final
    public partial class Compra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Se obtiene la lista de productos que se encuentran en el carrito
            //qye se va guardando en una variable de session. 
            CNegocio.CNCarritoCompra carrito = Session["Seleccion"] as CNegocio.CNCarritoCompra;
            if (Session["Logged"].ToString() == "Yes")
            {
                rptCompra.DataSource = carrito.obtenerProductos;
                rptCompra.DataBind();


            }
            else
            {
                this.btnBuyNow.Visible = false;
            }

        }


        //Este evento saca de la lista de productos temporales los items del carrito
        //por si el cliente se arrepiende de haberlo agregado.
        protected void rptCompra_ItemCommand(object source, RepeaterCommandEventArgs e)
        {


            int idProd = Convert.ToInt32(e.CommandArgument);
            CNegocio.CNCarritoCompra compra = Session["Seleccion"] as CNegocio.CNCarritoCompra;
            List<CEntidades.CEItemProducto> productos = Session["Productos"] as List<CEntidades.CEItemProducto>;

            CEntidades.CEItemProducto prod = productos.Find(p => p.id == idProd);

            compra.quitarProducto(prod);
            Session["Seleccion"] = compra;
            rptCompra.DataSource = compra.obtenerProductos;
            rptCompra.DataBind();
        }
        //Evento del boton que realiza la compra asignando la cantidad
        //de productos que quiere de cada item.
        protected void btnBuyNow_Click(object sender, EventArgs e)
        {
            CNegocio.CNCarritoCompra compra = Session["Seleccion"] as CNegocio.CNCarritoCompra;

            int cantidades = 0;
            int index = 0;
            foreach (RepeaterItem item in rptCompra.Items)
            {

                TextBox text = item.FindControl("txtQuantity") as TextBox;
                if (text != null && text.Text != "0")
                {
                    int cantidad = Convert.ToInt32(text.Text);
                    CEntidades.CEItemProducto p = compra.obtenerProductos[index];
                    p.cantidad = cantidad;
                    cantidades += cantidad;
                    index++;
                }

                Session["Seleccion"] = compra;
                Response.Redirect("CompraListaFinal.aspx");
            }

            if (cantidades == 0)
            {
                this.lblProductosValidator.Visible = true;
            }

        }

        protected void rptCompra_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {




        }

    }
}