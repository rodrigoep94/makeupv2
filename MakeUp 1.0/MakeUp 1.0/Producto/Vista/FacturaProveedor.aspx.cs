﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CNegocio;
using CEntidades;

namespace Producto.Vista
{
    public partial class FacturaProveedor : System.Web.UI.Page
    {
        //Creamos las instancias de la clase Eproducto y ProductoBol
        private int precioTotalFactura = 123123;
        private CEFacturaProveedor _facturaproveedor;
        private readonly CNFacturaProveedor _facturaproveedorBol = new CNFacturaProveedor();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ViewState["productosSeleccionados"] = new List<CEFacturaProveedorItem>();
                cargaComboProveedores();
                cargarComboEstados();
                cargarComboProductos();
            }
        }

        public void cargarComboEstados()
        {
            var facturaData = new CNFacturaProveedor();
            var estados = facturaData.GetEstadosFactura();
            this.DDLEstados.DataSource = estados;
            this.DDLEstados.DataTextField = "Nombre";
            this.DDLEstados.DataValueField = "Id";
            this.DDLEstados.DataBind();
            this.DDLEstados.Items.Insert(0, "Seleccione un estado");
            this.DDLEstados.SelectedIndex = 0;
        }

        public void cargaComboProveedores()
        {
            var proveedoresData = new CNProveedores();
            var proveedores = proveedoresData.GetProveedores();
            this.DDLProveedor.DataSource = proveedores;
            this.DDLProveedor.DataTextField = "nombre";
            this.DDLProveedor.DataValueField = "id";
            this.DDLProveedor.DataBind();
            this.DDLProveedor.Items.Insert(0, "Seleccione un proveedor");
            this.DDLProveedor.SelectedIndex = 0;
        }

        public void cargarComboProductos()
        {
            var cnProductos = new CNegocio.CNProductos();
            var productos = cnProductos.obtenerProductos();
            this.DDLProducto.DataSource = productos;
            this.DDLProducto.DataTextField = "pro_Nombre";
            this.DDLProducto.DataValueField = "id";
            this.DDLProducto.DataBind();
            this.DDLProducto.Items.Insert(0, "Seleccione un producto");
            this.DDLProducto.SelectedIndex = 0;
        }

        public void BusquedaProveedor(object sender, EventArgs e)
        {
            this.lblDatosProveedor.Visible = true;
            var dalProveedor = new CNProveedores();
            var proveedor = dalProveedor.GetProveedorById(Convert.ToInt32(DDLProveedor.SelectedValue));
            this.lblDatosProveedor.Text = $"CUIT: {proveedor.cuit}, Dirección: {proveedor.direccion}, Teléfono: {proveedor.telefono}";
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        protected void btnProducto_Click(object sender, EventArgs e)
        {
            if (ValidateProducto())
            {
                var producto = new CEFacturaProveedorItem()
                {
                    producto_id = Convert.ToInt32(DDLProducto.SelectedValue),
                    cantidad = Convert.ToInt32(txtCantidadProducto.Text),
                    precio_compra = Convert.ToInt32(txtPrecioCompraProducto.Text),
                    producto_nombre = DDLProducto.SelectedItem.Text
                };

                var productosSeleccionados = (List<CEFacturaProveedorItem>)ViewState["productosSeleccionados"];
                productosSeleccionados.Add(producto);
                ViewState["productosSeleccionados"] = productosSeleccionados;
                lblProductos.Visible = true;
                lblProductos.Text = $"Productos seleccionados (Total: ${productosSeleccionados.Sum(x => x.precio_compra * x.cantidad)})";
                ListView2.DataSource = productosSeleccionados;
                ListView2.DataBind();
                LimpiarControlesProducto();
            }
        }

        public void LimpiarControlesProducto()
        {
            DDLProducto.SelectedIndex = 0;
            txtCantidadProducto.Text = string.Empty;
            txtPrecioCompraProducto.Text = string.Empty;
        }

        public void LimpiarFormulario()
        {
            lblDatosProveedor.Visible = false;
            DDLEstados.SelectedIndex = 0;
            DDLProducto.SelectedIndex = 0;
            DDLProveedor.SelectedIndex = 0;
            txtCantidadProducto.Text = string.Empty;
            txtNumero.Text = string.Empty;
            txtPrecioCompraProducto.Text = string.Empty;
            hiddenDate.Value = string.Empty;
            lblProductos.Visible = false;
            ListView2.DataSource = new List<CEProducto>();
            ListView2.DataBind();
        }

        private void Guardar()
        {
            if (this.ValidateForm())
            {
                try
                {
                    if (_facturaproveedor == null) _facturaproveedor = new CEFacturaProveedor();

                    var bitacora = new CNBitacora();
                    var usuarioActual = (CEntidades.CEUsuario)Session["User"];

                    _facturaproveedor.fac_numero = txtNumero.Text;
                    _facturaproveedor.Id_fac_prov = Convert.ToInt32(DDLProveedor.SelectedValue);
                    _facturaproveedor.DV = Convert.ToInt32(0);
                    _facturaproveedor.fac_fecha_compra = Convert.ToDateTime(hiddenDate.Value);
                    _facturaproveedor.Id_fac_est = Convert.ToInt32(DDLEstados.SelectedValue);

                    var factura = _facturaproveedorBol.Registrar(_facturaproveedor);
                    bitacora.GuardarBitacora($"Se ha registrado exitosamente la factura con Id {factura.Id}", usuarioActual.NombreUsuario);

                    var proveedorItemDal = new CNFacturaProveedorItem();
                    var productos = (List<CEFacturaProveedorItem>)ViewState["productosSeleccionados"];
                    productos.ForEach(p =>
                    {
                        p.factura_id = factura.Id;
                        proveedorItemDal.Registrar(p);
                        bitacora.GuardarBitacora($"Se ha registrado el producto {p.producto_nombre} para la factura con id {factura.Id}", usuarioActual.NombreUsuario);
                    });

                    LimpiarFormulario();
                    lblSuccess.Text = "La factura ha sido de alta con exito";
                    lblSuccess.Visible = true;

                }
                catch (Exception ex)
                {
                    //MessageBox.Show(string.Format("Error: {0}", ex.Message), "Error inesperado");
                }
            }

        }

        public bool ValidateForm()
        {
            return ValidateFactura() && ValidateProductos();
        }

        public bool ValidateFactura()
        {
            var valid = true;
            var listDropdowns = new List<DropDownList>()
            {
                DDLProveedor,
                DDLEstados
            };

            var listLabels = new List<Label>()
            {
                lblProveedorValidator,
                lblEstadoCompraValidator
            };



            for (int i = 0; i < listLabels.Count(); i++)
            {
                if (listDropdowns.ElementAt(i).SelectedIndex == 0)
                {
                    listLabels.ElementAt(i).Visible = true;
                    valid = valid && false;
                }
                else
                {
                    listLabels.ElementAt(i).Visible = false;
                }
            }

            if (hiddenDate.Value == string.Empty)
            {
                lblFechaCompraValidator.Visible = true;
                valid = false;
            } else
            {
                lblFechaCompraValidator.Visible = false;
            }

            if (txtNumero.Text == string.Empty)
            {
                lblNumeroFacturaValidator.Visible = true;
                valid = false;
            } else
            {
                lblNumeroFacturaValidator.Visible = false;
            }

            return valid;
        }

        public bool ValidateProductos()
        {
            var valid = true;
            var productosSeleccionados = (List<CEFacturaProveedorItem>)ViewState["productosSeleccionados"];
            
            if(productosSeleccionados.Count() == 0)
            {
                lblProductosValidator.Visible = true;
                valid = false;
            } else
            {
                lblProductosValidator.Visible = false;
            }

            return valid;
        }

        public bool ValidateProducto()
        {
            var valid = true;

            var listForms = new List<TextBox>()
            {
                txtCantidadProducto,
                txtPrecioCompraProducto
            };

            var listLabels = new List<Label>()
            {
                lblCantidadProductoValidator,
                lblPrecioCompraValidator
            };

            for (int i = 0; i < listLabels.Count(); i++)
            {
                if (listForms.ElementAt(i).Text == string.Empty)
                {
                    listLabels.ElementAt(i).Visible = true;
                    valid = valid && false;
                }
                else
                {
                    listLabels.ElementAt(i).Visible = false;
                }
            }

            if (DDLProducto.SelectedIndex == 0)
            {
                lblProductoValidator.Visible = true;
                valid = valid && false;
            }
            else
            {
                lblProductoValidator.Visible = false;
            }

            return valid;
        }
    }
}