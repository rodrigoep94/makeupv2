﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site1.Master" CodeBehind="Notificaciones.aspx.cs" Inherits="Producto.Vista.Notificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="titulos">Notificaciones</h1>

    <asp:GridView ID="tableNotificaciones"
        runat="server"
        AutoGenerateColumns="False"
        BackColor="White"
        BorderColor="#3366CC"
        ShowHeaderWhenEmpty="true"
        EmptyDataText="No hay notificaciones para mostrar en este momento"
        BorderStyle="None"
        BorderWidth="1px"
        Style="width: 100%;"
        CellPadding="4">
        <RowStyle BackColor="White" ForeColor="#003399" />

        <Columns>
            <asp:TemplateField HeaderText="">
                <ItemTemplate>
                    <asp:LinkButton ID="btnEliminar" runat="server" CommandArgument='<%# Bind("id_notificacion")%>' OnClick="EliminarId">
                        ✘
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID">
                <ItemTemplate>
                    <asp:Label ID="LabelID" runat="server" Text='<%# Bind("id_notificacion") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mensaje">
                <ItemTemplate>
                    <asp:Label ID="LabelMensaje" runat="server" Text='<%# Bind("mensaje") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
    </asp:GridView>
</asp:Content>
