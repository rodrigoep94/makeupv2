﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CompraListaFinal.aspx.cs" Inherits="Producto.Vista.CompraListaFinal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<fieldset class="fieldsetclass">
 <legend>Productos Seleccionados</legend>
    <div id="divProductos" class="divProductos" runat="server">
        <asp:Repeater id="rptCompra" runat="server"  EnableViewState="False" 
            onitemdatabound="rptCompra_ItemDataBound" 
            onitemcommand="rptCompra_ItemCommand" >
            <HeaderTemplate>
                <table class="tableProducts">
                <tr>
                    <th class="thProduct">Imagen</th>
                    <th class="thProduct">Producto</th>
                    <th class="thProduct">Precio</th>
                    <th class="thProduct">Cantidad</th>
                    <th class="thProduct">Subtotal</th>
                </tr>
                
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="trProduct">
                    <td class="tdPoduct"><asp:Image ID="Image1" runat="server" ImageUrl= '<%# Eval("Imagen")%>' /></td>
                    <td class="tdPoduct"><%# Eval("nombre")%>  </td>
                    <td class="tdPoduct">$ <%# Eval("precio")%></td>
                    <td class="tdPoduct"><%# Eval("cantidad")%></td>
                    <td class="tdPoduct">$ <%# Eval("Subtotal")%></td>
                </tr>
            </ItemTemplate>
           <FooterTemplate>
                <tr>
                <th colspan="4" style="text-align: right">Total:</th>
                <td colspan="1" style="white-space: nowrap"><asp:TextBox ID="txtTotal" runat="server" Enabled="False"></asp:TextBox></td>
                <td></td>
                </tr>
                <tr>
                <th colspan="4" style="text-align: right"></th>
                <td colspan="1" style="white-space: nowrap"><asp:Button ID="Button1" runat="server" Text="Comprar" /></td>
                <td></td>
                </tr>
                </table>
            </FooterTemplate>
            
        </asp:Repeater>
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>


    </div>
</fieldset>
</asp:Content>
