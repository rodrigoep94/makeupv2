﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Producto.Vista
{
    public partial class BuzonMensajes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Populate the GridView. 
                BindGridView();
            }
        }

        private void BindGridView()
        {
            CEntidades.CEUsuario user = Session["User"] as CEntidades.CEUsuario;
            var cnMensajes = new CNegocio.CNMensajes();
            DataSet ds = cnMensajes.ObtenerMensajes(user.id);

            // Get the DataView from Person DataTable. 
            DataView dvMensajes = ds.Tables["Mensajes"].DefaultView;

            // Bind the GridView control. 
            tableMensajes.DataSource = dvMensajes;
            tableMensajes.DataBind();
        }

        protected void EliminarId(object sender, EventArgs e)
        {
            LinkButton lnkbtn = sender as LinkButton;

            var cnMensajes = new CNegocio.CNMensajes();
            var idMensajeAEliminar = Int32.Parse(lnkbtn.CommandArgument);
            cnMensajes.EliminarMensaje(idMensajeAEliminar);

            // Rebind the GridView control to show data after deleting. 
            BindGridView();
            Site1 MasterP = (Site1)this.Master;
            MasterP.UpdateMenu();
        }
    }
}