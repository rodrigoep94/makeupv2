﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FacturaProveedorItem.aspx.cs" Inherits="Producto.Vista.FacturaProveedorItem" %>

<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 67px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td class="style1">
                <asp:Label ID="lblNombre" runat="server" Text="Producto:" ForeColor="White"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DDLProveedor" runat="server">
                </asp:DropDownList>
            </td>
        </tr>

        <tr>
            <td class="style1">
                <asp:Label ID="lblNumero" runat="server" Text="Cantidad :" ForeColor="White"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNumero" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="btnUpload" runat="server" Text="Guardar Detalle de Factura"
                    OnClick="btnUpload_Click" />
            </td>
        </tr>
    </table>

    <div class="UploadImg">


        <asp:Label ID="Label1" runat="server" Text="" ForeColor="White" Visible="false"></asp:Label>

    </div>
</asp:Content>
