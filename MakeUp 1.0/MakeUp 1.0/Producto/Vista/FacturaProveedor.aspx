﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FacturaProveedor.aspx.cs" Inherits="Producto.Vista.FacturaProveedor" %>

<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 50%;
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="stylesheet" href="/resources/demos/style.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(document).ready(function () {
            $('input[id$=txtDate]').datepicker({
                dateFormat: 'dd/mm/yy'
            });
            var date = document.getElementById('<%= hiddenDate.ClientID %>').value;
            document.getElementById("txtDate").value = date;
        });

        $(function () {
            $("#txtDate").datepicker();
        });

        function changeDate() {
            var date = document.getElementById("txtDate").value;
            document.getElementById('<%= hiddenDate.ClientID %>').value = date;
        }
    </script>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblNombre" runat="server" Text="Nombre Proveedor:" ForeColor="White"></asp:Label>
                        <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                    <td>
                        <asp:DropDownList ID="DDLProveedor" runat="server" AutoPostBack="true" OnSelectedIndexChanged="BusquedaProveedor"></asp:DropDownList>
                        <asp:Label Text="El proveedor es requerido" ID="lblProveedorValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ForeColor="White" runat="server" ID="lblDatosProveedor" Visible="false"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td class="style1">
                        <asp:Label ID="lblNumero" runat="server" Text="Número de factura:" ForeColor="White"></asp:Label>
                        <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtNumero" runat="server" AutoPostBack="false"></asp:TextBox>
                        <asp:Label Text="El número de factura es requerido" ID="lblNumeroFacturaValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                </tr>

                <tr>
                    <td class="style1">
                        <asp:Label ID="lblFecha" runat="server" Text="Fecha de compra:" ForeColor="White"></asp:Label>
                        <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                    <td>
                        <input type="text" id="txtDate" onchange="changeDate()" />
                        <asp:HiddenField ID="hiddenDate" runat="server" Value="" />
                        <asp:Label Text="La fecha de compra es requerida" ID="lblFechaCompraValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                </tr>


                <tr>
                    <td class="style1">
                        <asp:Label ID="lblEstado" runat="server" Text="Estado de compra:" ForeColor="White"></asp:Label>
                        <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                    <td>
                        <asp:DropDownList ID="DDLEstados" runat="server" AutoPostBack="false"></asp:DropDownList>
                        <asp:Label Text="El estado de compra es requerido" ID="lblEstadoCompraValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div style="width: 100%">
        <fieldset class="fieldsetProducto">
            <legend>Agregar producto</legend>
            <table>
                <tr>
                    <td class="style1">
                        <asp:Label ID="lblProducto" runat="server" Text="Producto:" ForeColor="White"></asp:Label>
                        <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                    <td>
                        <asp:DropDownList ID="DDLProducto" runat="server" AutoPostBack="false"></asp:DropDownList>
                        <asp:Label Text="El producto es requerido" ID="lblProductoValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                </tr>

                <tr>
                    <td class="style1">
                        <asp:Label ID="lblCantidad" runat="server" Text="Cantidad:" ForeColor="White"></asp:Label>
                        <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCantidadProducto" runat="server" AutoPostBack="false"></asp:TextBox>
                        <asp:Label Text="La cantidad del producto es requerida" ID="lblCantidadProductoValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                </tr>

                <tr>
                    <td class="style1">
                        <asp:Label ID="lblPrecioCompra" runat="server" Text="Precio de compra:" ForeColor="White"></asp:Label>
                        <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtPrecioCompraProducto" runat="server" AutoPostBack="false"></asp:TextBox>
                        <asp:Label Text="El precio de compra del producto es requerido" ID="lblPrecioCompraValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Button ID="btnProducto" runat="server" Text="Agregar Producto"
                            OnClick="btnProducto_Click" />
                    </td>
                </tr>
            </table>

        </fieldset>
    </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <asp:Label Text="Debe agregar algún producto" ID="lblProductosValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
    <br />
    <asp:Label ID="lblProductos" runat="server" Visible="false" ForeColor="White"></asp:Label>

    <br />

    <asp:ListView ID="ListView2" runat="server">
        <LayoutTemplate>
            <table style="border: solid 2px #336699;" cellspacing="0" cellpadding="3" rules="all">
                <tr style="background-color: #336699; color: White;">
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Precio de Compra</th>
                </tr>
                <tbody>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr style="background-color: #dadada; color:#000000">
                <td><%# Eval("producto_nombre")%></td>
                <td><%# Eval("cantidad")%></td>
                <td><%# Eval("precio_compra")%></td>
            </tr>
        </ItemTemplate>
    </asp:ListView>

    <asp:HiddenField ID="hiddenTotal" runat="server" />

    <br />
    <br />

    <asp:Button ID="btnUpload" runat="server" Text="Guardar Factura"
        OnClick="btnUpload_Click" />


    <asp:Label ID="lblSuccess" runat="server" Text="" ForeColor="White" Visible="true"></asp:Label>
</asp:Content>
