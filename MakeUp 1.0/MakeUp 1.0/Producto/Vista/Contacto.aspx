﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Contacto.aspx.cs" Inherits="Producto.Vista.Contacto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--Script de goolge para mostrar la ubicacion de un lugar.--%>
 <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">
        var map;
        function initialize() {
            var myLatlng = new google.maps.LatLng(-34.771556, -58.404897);
            var myOptions = {
                zoom: 15,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map_canvas'),
            myOptions);

            var contentString = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1 id="firstHeading" class="firstHeading">Filomena Shoes</h1>' +
        '<div id="bodyContent">' +
        '<p>Filomena Calzados creo la primera línea ejecutiva casual del mercado.'+
        ' Utilizando gamuza suave en combinación con suelas de crepe ligero. ' +
        'En una época en que las opciones de calzado eran limitadas.' +
        ' En su constante innovación Filomena Calzados decide ingresar al mundo'+
        ' de las ventas mediante un nuevo canal. Internet.' +
        ' Relajado y clásico en el diseño Filomenta Calzados, siempre ha definido lo que significa ser casual y moderno.'+
        ' Hoy en día continúa innovando, brindando excelencia tecnologica y estilo genuino.</p>' +
        '</div>' +
        '</div>';

            var infowindow = new google.maps.InfoWindow({
                position: map.getCenter(),
                maxWidth: 500,
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: "Filomena Shoes"
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
        }

        


        google.maps.event.addDomListener(window, 'load', initialize);
        



    </script>
 <div id="map_canvas" style="width: 700px; height: 500px"></div>


</asp:Content>
