﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Compra.aspx.cs" Inherits="Producto.Vista.Compra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset class="fieldsetclass">
 <legend>Productos Seleccionados</legend>
    <div id="divProductos" class="divProductos" runat="server">
        <asp:Repeater id="rptCompra" runat="server"  
            onitemcommand="rptCompra_ItemCommand" EnableViewState="False" 
            onitemdatabound="rptCompra_ItemDataBound">
            <HeaderTemplate>
                <table class="tableProducts">
                <tr>
                    <th class="thProduct">Imagen</th>
                    <th class="thProduct">Producto</th>
                    <th class="thProduct">Precio</th>
                    <th class="thProduct">Cantidad</th>
                    <th class="thProduct"></th>
                </tr>
                
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="trProduct">
                    <td class="tdPoduct"><asp:Image ID="Image1" runat="server" ImageUrl= '<%# Eval("Imagen")%>' /></td>
                    <td class="tdPoduct"><%# Eval("nombre")%>  </td>
                    <td class="tdPoduct">$ <%# Eval("precio")%></td>
                    <td class="tdPoduct" style="white-space:nowrap"><asp:TextBox ID="txtQuantity" Text='<%# Eval("Cantidad")%>' Width="70px" runat="server"></asp:TextBox></td>
                    <td class="tdPoduct"><asp:Button ID="btnQuit" CommandArgument='<%# Eval("id")%>'  runat="server" Text="Quitar" /></td>
                    
                </tr>
            </ItemTemplate>
       
            
        </asp:Repeater>


    </div>
        <br />
        <br />
        <asp:Label Text="La cantidad de productos del carrito debe ser mayor a cero" ID="lblProductosValidator" Visible="false"  runat="server" Style="color: red; font-weight: bold" />
    
</fieldset>
<div style="text-align: center; margin-top: 20px">
<asp:Button ID="btnBuyNow" runat="server" Text="Comprar >>" 
        onclick="btnBuyNow_Click" />
</div>
</asp:Content>
