﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AbmProductos.aspx.cs" Inherits="Producto.Vista.AbmProductos" %>

<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 29%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td class="style1">
                <asp:Label ID="lblNombre" runat="server" Text="Nombre :" ForeColor="White">
                <asp:Label Text="*" runat="server" style="color:red; font-weight:bold" />
                </asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                <asp:Label Text="El nombre es requerido" ID="lblNombreValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>


        <tr>
            <td class="style1">
                <asp:Label ID="lblImg" runat="server" Text="Imagen :" ForeColor="White">
                <asp:Label Text="*" runat="server" style="color:red; font-weight:bold"/>
                </asp:Label>
            </td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" ForeColor="White" />
                <asp:Label Text="La imagen es requerida" ID="lblImagenValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>

        <tr>
            <td class="style1">
                <asp:Label ID="lblPrecio" runat="server" Text="Precio de venta:" ForeColor="White">
                <asp:Label Text="*" runat="server" style="color:red; font-weight:bold"/>
                </asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtPrecio" runat="server"></asp:TextBox>
                <asp:Label Text="El precio es requerido" ID="lblPrecioValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>
        <tr>
            <td class="style1">
                <asp:Label ID="lblCantidad" runat="server" Text="Cantidad mínima:" ForeColor="White">
                <asp:Label Text="*" runat="server" style="color:red; font-weight:bold"/>
                </asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtCantidad" runat="server"></asp:TextBox>
                <asp:Label Text="La cantidad mínima es requerida" ID="lblCantidadValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>

        <tr>
            <td class="style1">
                <asp:Label ID="lblStock" runat="server" Text="Stock:" ForeColor="White">
                <asp:Label Text="*" runat="server" style="color:red; font-weight:bold"/>
                </asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtStock" runat="server"></asp:TextBox>
                <asp:Label Text="El stock es requerido" ID="lblStockValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>


        <tr>
            <td></td>
            <td>
                <asp:Button ID="btnUpload" runat="server" Text="Alta Producto"
                    OnClick="btnUpload_Click" />
            </td>
        </tr>
    </table>



    <asp:Label ID="Label1" runat="server" Text="" ForeColor="White" Visible="false"></asp:Label>

</asp:Content>
