﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CNegocio;
using CEntidades;

namespace Producto.Vista
{
    public partial class Proveedores : System.Web.UI.Page
    {
        private CEProveedores _proveedor;
        //Creamos las instancias de la clase Eproducto y ProductoBol
        private readonly CNProveedores _proveedorinsertar = new CNProveedores();
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            var bitacora = new CNBitacora();
            try
            {
                if (this.ValidateForm())
                {

                    var proveedor = new CEProveedores()
                    {
                        nombre = txtNombre.Text,
                        direccion = txtdireccion.Text,
                        telefono = Convert.ToInt32(txtTelefono.Text),
                        cuit = Convert.ToInt32(txtCuit.Text)
                    };


                    CEntidades.CEUsuario user = Session["User"] as CEntidades.CEUsuario;
                    var proveedorInsertado = _proveedorinsertar.Registrar(proveedor);
                    bitacora.GuardarBitacora($"El proveedor con id {proveedorInsertado.id} ha sido dado de alta con éxito", user.NombreUsuario);


                    this.Label1.Text = "El proveedor fue dado de alta con éxito!";
                    this.Label1.Visible = true;
                    this.ResetForm();
                }
            }
            catch (Exception ex)
            {
                this.Label1.Text = "Error al registrar el proveedor";
                this.Label1.Visible = true;
                bitacora.GuardarBitacora("Hubo un error al intentar guadar un proveedor", Session["User"].ToString());
            }
        }

        protected Boolean ValidateForm()
        {

            var listLabels = new List<Label>()
            {
                lblNombreValidator,
                lblDireccionValidator,
                lblCuitValidator,
                lblTelefonoValidator
            };

            var listForms = new List<TextBox>()
            {
                txtNombre,
                txtdireccion,
                txtCuit,
                txtTelefono
            };

            var valid = true;

            for (int i = 0; i < listLabels.Count(); i++)
            {
                if (listForms.ElementAt(i).Text == string.Empty)
                {
                    listLabels.ElementAt(i).Visible = true;
                    valid = valid && false;
                }
                else
                {
                    listLabels.ElementAt(i).Visible = false;
                }
            }

            return valid;
        }

        private void ResetForm()
        {
            txtCuit.Text = string.Empty;
            txtdireccion.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtTelefono.Text = string.Empty;
        }
    }
}
