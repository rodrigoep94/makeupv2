﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Bitacora.aspx.cs" Inherits="Producto.Vista.Bitacora" %>

<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 50%;
        }
    </style>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

        $(document).ready(function () {
            var input = $('input[id$=txtDateDesde]');
            var inputH = $('input[id$=txtDateHasta]');
            input.datepicker({
                dateFormat: 'dd/mm/yy'
            });
            inputH.datepicker({
                dateFormat: 'dd/mm/yy'
            });
            var date = document.getElementById('<%= hiddenDateDesde.ClientID %>').value;
            document.getElementById("txtDateDesde").value = date;
            var date = document.getElementById('<%= hiddenDateHasta.ClientID %>').value;
            document.getElementById("txtDateHasta").value = date;
        });

        $(function () {
            $("#txtDateDesde").datepicker();
            $("#txtDateHasta").datepicker();
        });

        function changeDate() {
            var date = document.getElementById("txtDateDesde").value;
            document.getElementById('<%= hiddenDateDesde.ClientID %>').value = date;
            date = document.getElementById("txtDateHasta").value;
            document.getElementById('<%= hiddenDateHasta.ClientID %>').value = date;
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblNombre" runat="server" Text="Usuario:" ForeColor="White"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DDLUsuario" runat="server" AutoPostBack="true"></asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="lblNumero" runat="server" Text="Criticidad:" ForeColor="White"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DDLCriticidad" runat="server" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="lblFechaDesde" runat="server" Text="Fecha Desde:" ForeColor="White"></asp:Label>
                    </td>
                    <td>
                        <input type="text" id="txtDateDesde" onchange="changeDate()" />
                        <asp:HiddenField ID="hiddenDateDesde" runat="server" Value="" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <asp:Label ID="lblFechaHasta" runat="server" Text="Fecha Hasta:" ForeColor="White"></asp:Label>
                    </td>
                    <td>
                        <input type="text" id="txtDateHasta" onchange="changeDate()" />
                        <asp:HiddenField ID="hiddenDateHasta" runat="server" Value="" />
                    </td>
                </tr>


                <tr>
                    <td>
                        <asp:Label ID="lblEstado" runat="server" Text="Descripción:" ForeColor="White"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DDLDescripcion" runat="server" AutoPostBack="false"></asp:DropDownList>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />

    <br />
    <br />


    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
        Width="600px" OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
        CssClass="GridViewStyle" AllowSorting="True" GridLines="None"
        AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging"
        PageSize="10" Height="407px">
        <Columns>
            <asp:BoundField DataField="bit_id" HeaderText="Id" />
            <asp:BoundField DataField="bit_fun" HeaderText="Descripcion" />
            <asp:BoundField DataField="bit_usu" HeaderText="Usuario" />
            <asp:BoundField DataField="bit_Fecha" HeaderText="Fecha" />
            <asp:BoundField DataField="bit_Criticidad" HeaderText="Criticidad" />
        </Columns>
        <HeaderStyle CssClass="HeaderStyle" />
        <PagerStyle CssClass="PagerStyle" />
        <RowStyle CssClass="RowStyle" />
        <SelectedRowStyle CssClass="SelectedRowStyle" />
        <EditRowStyle CssClass="EditRowStyle" />
        <AlternatingRowStyle CssClass="AltRowStyle" />
    </asp:GridView>
</asp:Content>
