﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Producto.Vista
{
    public partial class RestaurarDigitosVerificadores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRestaurardv_Click(object sender, EventArgs e)
        {
            //Obtengo todas las tablas que hay que comprobar los DVH y DVV.
            string queryTable = "select * from digver";
            DataTable table = new DataTable("DVVertical");
            //Me conecto a la base de datos en modo desconectado y lleno un datable para ir recorriendo los resultados.
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(queryTable, conn);
            table.Clear();
            da.Fill(table);
            //Recorro los items de la tabla.
            foreach (DataRow item in table.Rows)
            {
                string tableName = item["tabla"].ToString();
                // int DVV = Convert.ToInt32(item["dvv"]);
                //Metodo que realiza el calculo del digito verificado por tabla. 
                var cnDigVer = new CNegocio.CNDigVer();
                cnDigVer.actualizarDVH(tableName);
                //Dependiendo el tipo de la clase que tiene en este momento (Patron State) devuelvo su mensaje de estado.
            }
            conn.Close();
        }
        
    }
}