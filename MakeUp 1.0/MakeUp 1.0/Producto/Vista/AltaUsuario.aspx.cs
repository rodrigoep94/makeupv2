﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cnegocio;

namespace Producto.Vista
{
    public partial class AltaUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CEntidades.CEUsuario user = Session["User"] as CEntidades.CEUsuario;
            if (user.TipoUsuario != 1)
            {

                Response.Redirect("Logout.aspx?message=Usted no posee los permisos necesarios para acceder a esta pagina");

            }

        }

        protected void CreateUser_CreatedUser(object sender, EventArgs e)
        {



        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (this.TextBox1.Text != "" || this.TextBox2.Text != "" || this.TextBox3.Text != "" || this.TextBox4.Text != "")
            {
                string nombre = this.TextBox1.Text;
                string apellido = this.TextBox2.Text;
                string usuario = this.TextBox3.Text;
                string pass = this.TextBox4.Text;
                int tipoUser = Convert.ToInt32(this.DropDownList1.SelectedValue);

                var cnLogin = new CNegocio.CNLogin();
                if (!cnLogin.existeUsuario(usuario))
                {
                    CEntidades.CEUsuario user = new CEntidades.CEUsuario();

                    user.Nombre = nombre;
                    user.Apellido = apellido;
                    user.NombreUsuario = usuario;
                    user.Password = pass;
                    user.TipoUsuario = tipoUser;

                    var cnUsuario = new CNegocio.CNUsuario();
                    user = cnUsuario.Insert(user);


                    if (user.id != 0)
                    {
                        this.Label1.Text = "El usuario se creó de forma exitosa";
                        this.Label1.Visible = true;
                    }
                }
                else
                {
                    this.Label1.Text = "Es nombre de usuario ya ha sido utilizado por otra persona";
                    this.Label1.Visible = true;
                }


            }
            else
            {
                this.Label1.Text = "Todos los campos son obligatorios";
                this.Label1.Visible = true;
            }

        }
    }
}