﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Producto.Vista
{
    //Pagina que le muestra al usuario que no sea el webmaster cuando
    //existe un error de digito verificador y no lo deja hacer nada
    //ya que no hereda de la masterpage y no contiene el menu navegable.
    public partial class Logout2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Label2.Text = Request.QueryString["message"];
            this.Label2.ForeColor = System.Drawing.Color.White;
        }
    }
}