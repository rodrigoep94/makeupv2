﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CNegocio;
using CEntidades;

namespace Producto.Vista
{
    public partial class FacturaProveedorItem : System.Web.UI.Page
    {
        //Creamos las instancias de la clase Eproducto y ProductoBol
        private CEFacturaProveedorItem _facturaproveedoritem;
        private readonly CNFacturaProveedorItem _facturaproveedorBolitem = new CNFacturaProveedorItem();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                cargaDatos();
            }

        }

        public void cargaDatos()
        {

            DataTable table = new DataTable();
            table = CNegocio.CNProductos.obtenerFiltroProducto();
            //table = CNegocio.CNBitacora.obtenerFiltro("bitacora", "bit_usu");
            this.DDLProveedor.DataSource = table;
            this.DDLProveedor.DataTextField = "bit_usu";
            this.DDLProveedor.DataValueField = "bit_usu";
            this.DDLProveedor.DataBind();
            this.DDLProveedor.Items.Insert(0, "");
            this.DDLProveedor.SelectedIndex = 0;


            //List<string> Prov = new List<string>();

            //Prov = CNProveedores.Get_Proveedores_All();
            //for (int i = 0; i < Prov.Count; i++)
            //{
            //    DDLProveedor.Items.Add(Prov[i]);
            //}

            //DataTable table = new DataTable();
            //table = CNegocio.CNBitacora.obtenerBitacora();
            //this.GridView1.DataSource = table;
            //this.GridView1.AllowPaging = true;

            //this.GridView1.DataBind();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            Guardar();
        }
        private void Guardar()
        {
            try
            {
                if (_facturaproveedoritem == null) _facturaproveedoritem = new CEFacturaProveedorItem();

                _facturaproveedoritem.factura_id = Convert.ToInt32(1);
                _facturaproveedoritem.producto_id = Convert.ToInt32(1);
                _facturaproveedoritem.precio_compra = Convert.ToInt32(1);
                _facturaproveedoritem.cantidad = Convert.ToInt32(10);
                _facturaproveedorBolitem.Registrar(_facturaproveedoritem);

                Response.Redirect("FacturaProveedor.aspx");

                //_facturaproveedor.Id = Convert.ToInt32(txtId.Text);
                //_facturaproveedoritem.cantidad .fac_numero = txtNumero.Text;
                //_facturaproveedoritem.Id_fac_prov = Convert.ToInt32(DDLProveedor.SelectedItem);
                //_facturaproveedoritem.DV = Convert.ToInt32(0);


                //if (_facturaproveedorBol.stringBuilder.Length != 0)
                //{
                //    MessageBox.Show(_facturaproveedorBol.stringBuilder.ToString(), "Para continuar:");
                //}
                //else
                //{
                //    MessageBox.Show("Producto registrado/actualizado con éxito");

                //}
            }
            catch (Exception ex)
            {
                //MessageBox.Show(string.Format("Error: {0}", ex.Message), "Error inesperado");
            }

        }

    }
}