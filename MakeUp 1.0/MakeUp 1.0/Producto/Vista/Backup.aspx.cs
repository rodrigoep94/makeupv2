﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocio;

namespace Producto.Vista
{
    public partial class Backup : System.Web.UI.Page
    {
        //Load de la pagina que inicializa el label de estado del backup para que no sea visible.
        //en caso de que haya un error o finalizacion correcta el label se vuelve visible
        //mostrando el mensaje de error o finalizacion correcta.
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Visible = false;
        }
        //Boton que realiza en evento para generar el backup.
        protected void Button1_Click(object sender, EventArgs e)
        {

            string nombre = TextBox1.Text;
            string message = "";
            var cnDatabase = new CNegocio.CNDatabase();
            bool isOk = cnDatabase.RealizarBackup(nombre, ref message);

            if (isOk)
            {

                Label1.Text = "El backup se realizó correctamente";
            }
            else
            {
                Label1.Text = "Hubo un error al realizar el backup : " + message;
            }
            Label1.Visible = true;
        }
    }
}