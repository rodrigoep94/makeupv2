﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CNegocio;

namespace Producto.Vista
{
    public partial class EnviarMensaje : System.Web.UI.Page
    {
        private CEntidades.CEUsuario loggedUser = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != "")
            {
                loggedUser = (CEntidades.CEUsuario)Session["User"];
                txtNombre.Text = loggedUser.Nombre;
                txtNombre.Enabled = false;
            }

            if (!Page.IsPostBack)
            {
                var usuarios = new CNegocio.CNUsuario();
                DataTable dt = usuarios.GetUsers();
                ddlDestinatario.DataSource = dt;
                ddlDestinatario.DataTextField = "usu_nombre";
                ddlDestinatario.DataValueField = "id";
                ddlDestinatario.DataBind();

                ListItem item = new ListItem();
                item.Text = "Seleccionar un usuario";
                item.Value = "0";

                this.ddlDestinatario.Items.Insert(0, item);
            }
        }


        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if (this.ValidateForm())
            {
                CEntidades.CEMensaje m = new CEntidades.CEMensaje();
                if (loggedUser != null) m.Emisor_Id = loggedUser.id;
                m.Emisor = txtNombre.Text;
                m.Destinatario = ddlDestinatario.SelectedItem.Text;
                m.Destinatario_Id = Int32.Parse(ddlDestinatario.SelectedItem.Value);
                m.Mensaje = txtMensaje.Text;
                m.FechaEnvio = DateTime.Today;

                var cnMensaje = new CNMensajes();
                m = cnMensaje.InsertarMensaje(m);

                if (m.Id != 0)
                {
                    this.Label1.Text = "El mensaje fue enviado con éxito!";
                    this.Label1.Visible = true;
                    this.ResetForm();
                }
                else
                {
                    this.Label1.Text = "Error al enviar el mensaje";
                    this.Label1.Visible = true;
                }
            }

        }


        protected bool ValidateForm()
        {
            var listLabels = new List<Label>()
            {
                lblNombreValidator,
                lblMensajeValidator
            };

            var listForms = new List<TextBox>()
            {
                txtNombre,
                txtMensaje
            };

            var valid = true;

            for (int i = 0; i < listLabels.Count(); i++)
            {
                if (listForms.ElementAt(i).Text == string.Empty)
                {
                    listLabels.ElementAt(i).Visible = true;
                    valid = valid && false;
                }
                else
                {
                    listLabels.ElementAt(i).Visible = false;
                }
            }

            if (ddlDestinatario.SelectedItem.Value == "0")
            {
                lblDestinatarioValidator.Visible = true;
                valid = valid && false;
            }

            return valid;

        }

        private void ResetForm()
        {
            ddlDestinatario.ClearSelection();
            ddlDestinatario.Items.FindByValue("0").Selected = true;
            txtNombre.Text = string.Empty;
            txtMensaje.Text = string.Empty;
        }
    }
}