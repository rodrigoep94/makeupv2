﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Proveedores.aspx.cs" Inherits="Producto.Vista.Proveedores" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Contentplaceholder2" runat="server">
</asp:Content>--%>

<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            width: 67px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td class="style1">
                <asp:Label ID="lblNombre" runat="server" Text="Nombre :" ForeColor="White"></asp:Label>
                <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
            </td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
                <asp:Label Text="El nombre es requerido" ID="lblNombreValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>

        <tr>
            <td class="style1">
                <asp:Label ID="lbldireccion" runat="server" Text="Direccion :" ForeColor="White"></asp:Label>
                <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
            </td>
            <td>
                <asp:TextBox ID="txtdireccion" runat="server"></asp:TextBox>
                <asp:Label Text="La dirección es requerida" ID="lblDireccionValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>


        <tr>
            <td class="style1">
                <asp:Label ID="lblCuit" runat="server" Text="CUIT :" ForeColor="White"></asp:Label>
                <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
            </td>
            <td>
                <asp:TextBox ID="txtCuit" runat="server" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);"></asp:TextBox>
                <asp:Label Text="El CUIT es requerido" ID="lblCuitValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>


        <tr>
            <td class="style1">
                <asp:Label ID="lblTelefono" runat="server" Text="Teléfono :" ForeColor="White"></asp:Label>
                <asp:Label Text="*" runat="server" Style="color: red; font-weight: bold" />
            </td>
            <td>
                <asp:TextBox ID="txtTelefono" runat="server" onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32);"></asp:TextBox>
                <asp:Label Text="El número de teléfono es requerido" ID="lblTelefonoValidator" Visible="false" runat="server" Style="color: red; font-weight: bold" />
            </td>
        </tr>


        <tr>
            <td>
                <asp:Button ID="btnUpload" runat="server" Text="Alta Proveedor" OnClick="btnUpload_Click" />
            </td>
        </tr>
    </table>

    <div class="UploadImg">


        <asp:Label ID="Label1" runat="server" Text="" ForeColor="White" Visible="false"></asp:Label>

    </div>
</asp:Content>
