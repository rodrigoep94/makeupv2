﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site1.Master" CodeBehind="Notificaciones.aspx.cs" Inherits="Producto.Vista.ListaCompras" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Styles/Style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 class="titulos">Historial de ventas</h1>

    <asp:GridView ID="tableVentas"
        runat="server"
        AutoGenerateColumns="False"
        BackColor="White"
        BorderColor="#3366CC"
        ShowHeaderWhenEmpty="true"
        EmptyDataText="No hay ventas para mostrar en este momento"
        BorderStyle="None"
        BorderWidth="1px"
        Style="width: 100%;"
        CellPadding="4">
        <RowStyle BackColor="White" ForeColor="#003399" />

        <Columns>
            <asp:TemplateField HeaderText="ID">
                <ItemTemplate>
                    <asp:Label ID="LabelID" runat="server" Text='<%# Bind("id_venta") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fecha">
                <ItemTemplate>
                    <asp:Label ID="LabelMensaje" runat="server" Text='<%# Bind("fecha_venta") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Detalle">
                <ItemTemplate>
                    <asp:Label ID="LabelMensaje" runat="server" Text='<%# Bind("Detalle") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
        <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
        <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
    </asp:GridView>
</asp:Content>
