﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Producto.Vista
{
    public partial class Notificaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Populate the GridView. 
                BindGridView();
            }
        }

        private void BindGridView()
        {
            var cnNotificaciones = new CNegocio.CNNotificaciones();
            DataSet ds = cnNotificaciones.ObtenerNotificaciones();

            // Get the DataView from Person DataTable. 
            DataView dvNotificaciones = ds.Tables["Notificaciones"].DefaultView;

            // Bind the GridView control. 
            tableNotificaciones.DataSource = dvNotificaciones;
            tableNotificaciones.DataBind();
        }

        protected void EliminarId (object sender, EventArgs e)
        {
            LinkButton lnkbtn = sender as LinkButton;

            var cnNotificaciones = new CNegocio.CNNotificaciones();
            var idNotificacionAEliminar = Int32.Parse(lnkbtn.CommandArgument);
            cnNotificaciones.EliminarNotificacion(idNotificacionAEliminar);

            // Rebind the GridView control to show data after deleting. 
            BindGridView();
            Site1 MasterP = (Site1)this.Master;
            MasterP.UpdateMenu();
        }
    }
}