﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using CNegocio;

namespace Producto.Vista
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login2_Authenticate(object sender, AuthenticateEventArgs e)
        {
            //Evento del asp:login que se ejecuta cuando se hace click en iniciar sesion.
            //Busco en la clase del login si el usuario que se ingreso existe.
            // if (Dal.Login.existeUsuario(Login2.UserName))
            var cnLogin = new CNegocio.CNLogin();
            if (cnLogin.existeUsuario(Login2.UserName))
            {
                //Si el usuario existe obtengo el objeto usuario con todos los datos de la base de datos.

                CEntidades.CEUsuario user = cnLogin.Autenticar(Login2.UserName, Login2.Password);
                //Si el usuario es null quiere decir que la contraseña que ingreso no es la correcta.
                if (user != null)
                {
                    //Metodo de auntenticacion para el nombre de usuario proporcionado y poder cambiarle el estado
                    //en el loginView que se ingreso en la Master Page.

                    FormsAuthentication.SetAuthCookie(Login2.UserName, false);
                    //Modifico las variables de Sesion generadas en el global.asax y le cambio el valor.
                    //Para luego realizar validacion con respecto a los permisos por usuarios y mantener la sesion.
                    Session["Logged"] = "Yes";
                    Session["User"] = user;
                    CNCarritoCompra carrito = new CNCarritoCompra();
                    Session["Seleccion"] = carrito;



                    //Compruebo los digitos verificadores en la base de  datos, Dependiendo el estado de la misma redirecciono
                    //a una determinada página. 
                    CNegocio.CNDigVer dv = new CNegocio.CNDigVer();
                    CNegocio.State estado = dv.ComprobarDV();
                    Session["STATE"] = estado;
                    //Utilizo el patron State y Singleton para realizar estos calculos. Si el estado de la base no es Optima redirecciono a
                    //un pagina para mostrar el tipo del error en la base de datos y mostrarlo por pantalla.
                    string typeName = estado.GetType().Name;
                    if (!typeName.Equals("Optimo"))
                    {
                        if (user.TipoUsuario != 1)
                        {
                            Session["Logged"] = "No";
                            string mensaje = "Hubo un problema, Contactese con el Webmaster";
                            string redirect = String.Format("Logout.aspx?message={0}", mensaje);
                            Response.Redirect(redirect, true);
                        }
                        else
                        {
                            string mensaje = "";
                            if (typeName.Equals("ErrorRegistro"))
                            {
                                mensaje = estado.DVState() + " Tabla : " + estado.tabla + " Registro :" + estado.registro;
                            }
                            else
                            {
                                mensaje = estado.DVState() + " Tabla : " + estado.tabla;
                            }

                            string redirect = String.Format("LogoutAdmin.aspx?message={0}", mensaje);
                            Response.Redirect(redirect, true);
                        }



                    }
                    else
                    {
                        //Reinicio el contador de ingresos incorrectos a 0 siempre que el usuario ingrese bien al sistema.
                        cnLogin.reiniciarContador(user);
                        //En caso de que los digitos verificadores se comprobaran correctamente redirecciono al main para que se 
                        //le den los permisos que le corresponde al tipo de usuario.


                        //comente estas lineas para detectar problemas
                        // CarritoCompra compra = new CarritoCompra();
                        //Session["Seleccion"] = compra;
                        Response.Redirect("main.aspx", true);
                    }



                }
                else
                {
                    //Si el usuario no ingreso la contraseña correctamente, se aumenta el contador de ingresos incorrectos.
                    //El metodo AumentarContadore devuelve luego de aumentar el CII si el usuario se ha bloqueado o no.
                    bool estaBloqueado = cnLogin.AumentarContador(Login2.UserName);
                    //Si esta bloqueado se le asigna al asp:Login el mensaje de usuario bloqueado.
                    if (estaBloqueado)
                    {

                        Login2.FailureText = "El usuario esta bloqueado por favor contactese con el administrador";
                    }
                    else
                    {
                        //Si se ingreso incorrectamente la contraseña y no esta bloqueado se le asigna al asp:Login el mensaje contraseña invalida.
                        Login2.FailureText = "Contraseña Inválida";
                    }
                }

            }
            else
            {
                //En caso de que el usuario ingresado no exista en la base de datos asigno al asp:Login el mensaje de Usuario Inexistente.
                Login2.FailureText = "Usuario inexistente";
            }


        }

    }
}
//        protected void Page_Load(object sender, EventArgs e)
//        {

//        }

//        protected void btnEnviar_Click(object sender, EventArgs e)
//        {
//            //encriptamos la cadena inicial       
//            //txtPass.Text = Controlador.Encriptar.encriptando(txtUser.Text);
//            //ahora desencriptamos
//            txtUser.Text = Controlador.Encriptar.desencriptando(txtPass.Text);

//        }
//    }
//}