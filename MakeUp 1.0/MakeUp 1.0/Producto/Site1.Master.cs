﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using CNegocio;

namespace Producto
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        //<summary>
        //Esta página es la que contendrá el formato que van a heredar las paginas hijas de la Master Page y contiene los siguientes elementos:
        // •	Asp:Menu (Menu que luego se darán o sacar permisos dependiendo el perfil)
        // •	Asp:LoginView (Para permitir al usuario loguearse o desloguearse de la web)
        // •	Asp:ContentPlaceHolder (Para que lo hereden las demás paginas que dependan de la MasterPage)
        //</sumary>


        //Lista de permisos que se aplicaran dependiento el tipo de usuario.
        private List<MenuItem> lista = null;
        //Enum para establecer el tipo de usuario y mostrarlo por pantalla.
        private enum usersTypes
        {
            WebMaster = 1, Admin = 2, Cliente = 3
        }
        //Metodo que se acciona cada vez que se carga cada página.
        protected void Page_Load(object sender, EventArgs e)
        {

            if (HttpContext.Current != null && HttpContext.Current.Session != null && Session["appLang"] != null)
            {
                var lang = Session["appLang"].ToString();
                ddlLanguage.Items.FindByValue(lang).Selected = true;
            }
            //Si el usuario esta logueado y auntenticado le cargo el menu correspondiente
            //al tipo de usuario cada vez que se carga cada página, ya que el menu se encuentra en la MasterPage.
            if (LoginView1.Page.User.Identity.IsAuthenticated)
            {
                Session["Logged"] = "Yes";
                var userName = LoginView1.Page.User.Identity.Name;
                var cnUsuario = new CNegocio.CNUsuario();
                Session["User"] = cnUsuario.GetUserByName(userName);
            }

            if (LoginView1.Page.User.Identity.IsAuthenticated && Session["Logged"].Equals("Yes"))
            {
                //Obtengo el usuario que se guardo cuando se inicio sesion.
                CEntidades.CEUsuario user = Session["User"] as CEntidades.CEUsuario;
                //Llamos al método crearMenu para cargar los items del menu correspondiente al tipo de usuario.
                createMenu(user.TipoUsuario);
                //Dependiendo el tipo de usuario le asigno el valor a un label para mostrarlo por pantalla.

                switch (user.TipoUsuario)
                {
                    case (int)usersTypes.WebMaster:
                        this.Label1.Text = "WebMaster";
                        return;
                    case (int)usersTypes.Admin:
                        this.Label1.Text = "Admin";
                        return;
                    case (int)usersTypes.Cliente:
                        this.Label1.Text = "Cliente";
                        return;
                }
            }
            else
            {
                CNCarritoCompra carrito = null;
                Session["Seleccion"] = carrito;
                Session["Logged"] = "No";
            }
        }

        protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {

            Session["appLang"] = hdLanguage.Value;
            ddlLanguage.Items.FindByValue(hdLanguage.Value).Selected = true;
            Response.Redirect(Request.RawUrl);
        }

        public void UpdateMenu()
        {
            foreach (MenuItem i in this.Menu1.Items)
            {
                if (i.Text.Contains("Notificaciones"))
                {
                    var cantidadNotificaciones = this.GetCantidadNotificaciones();
                    if (cantidadNotificaciones > 0)
                    {
                        i.Text = $"Notificaciones  <font color = red> { cantidadNotificaciones } </font>";
                    }
                    else
                    {
                        i.Text = "Notificaciones";
                    }
                }
                if (i.Text.Contains("Buzon de mensajes"))
                {
                    var cantidadMensajes = this.GetCantidadMensajes();
                    if (cantidadMensajes > 0)
                    {
                        i.Text = $"Buzon de mensajes  <font color = red> { cantidadMensajes } </font>";
                    }
                    else
                    {
                        i.Text = "Buzon de mensajes";
                    }
                }
            }
        }

        public void createMenu(int userType)
        {
            //Busco los permisos correspondientes al tipo de usuario que se encuentra en la base de datos
            //en la tabla MenuXTipoUsuario.   
            var cnLogin = new CNegocio.CNLogin();
            lista = cnLogin.SetPermisos(userType);

            if (this.Menu1.Items.Count != lista.Count + 4)
            {
                //Recorro los item del menú.
                foreach (MenuItem i in lista)
                {
                    if (i.Text == "Notificaciones")
                    {
                        var cantidadNotificaciones = this.GetCantidadNotificaciones();
                        if (cantidadNotificaciones > 0) i.Text = $"Notificaciones  <font color = red> { cantidadNotificaciones } </font>";
                    }

                    if (i.Text == "Buzon de mensajes")
                    {
                        var cantidadMensajes = this.GetCantidadMensajes();
                        if (cantidadMensajes > 0) i.Text = $"Buzon de mensajes  <font color = red> { cantidadMensajes } </font>";
                    }
                    //Agrego lo items a la lista para que se vean en la pantalla.
                    this.Menu1.Items.Add(i);
                }
            }
        }
        //Evento que se desencadena cuando un usuario hace click en un item del menu y redirecciono a la pagina correspondiente.
        protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "Quienes Somos":
                    Response.Redirect("main.aspx");
                    return;
                case "Productos":
                    Response.Redirect("Productos.aspx");
                    return;
                case "Crear Usuario":
                    Response.Redirect("AltaUsuario.aspx");
                    return;
                case "Bitacora":
                    Response.Redirect("Bitacora.aspx");
                    return;
                case "Backup":
                    Response.Redirect("Backup.aspx");
                    return;
                case "Restore":
                    Response.Redirect("Restore.aspx");
                    return;
                case "Alta de Productos":
                    Response.Redirect("AbmProductos.aspx");
                    return;
                case "Alta de Proveedores":
                    Response.Redirect("Proveedores.aspx");
                    return;
                case "Contacto":
                    Response.Redirect("Contacto.aspx");
                    return;
                case "EnviarMensaje":
                    Response.Redirect("EnviarMensaje.aspx");
                    return;
                case "Restaurar DV":
                    Response.Redirect("RestaurarDigitosVerificadores.aspx");
                    return;
                case "Factura Proveedor":
                    Response.Redirect("FacturaProveedor.aspx");
                    return;
                case "Notificaciones":
                    Response.Redirect("Notificaciones.aspx");
                    return;
                case "Historial de ventas":
                    Response.Redirect("ListaCompras.aspx");
                    return;
                case "Buzon de mensajes":
                    Response.Redirect("BuzonMensajes.aspx");
                    return;
            }
        }

        protected void LoginView1_Unload(object sender, EventArgs e)
        {

        }

        private int GetCantidadNotificaciones()
        {
            var cnNotificaciones = new CNegocio.CNNotificaciones();
            return cnNotificaciones.GetCantidadNotificaciones();
        }

        private int GetCantidadMensajes()
        {
            CEntidades.CEUsuario user = Session["User"] as CEntidades.CEUsuario;
            var cnMensajes = new CNegocio.CNMensajes();
            return cnMensajes.GetCantidadMensajes(user.id);
        }
    }
}