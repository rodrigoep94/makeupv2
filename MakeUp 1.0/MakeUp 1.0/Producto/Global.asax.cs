﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Producto
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            string culture = "es-ES";

            if (HttpContext.Current != null && HttpContext.Current.Session != null &&  Session["appLang"] != null)
            {
                culture = Session["appLang"].ToString();
                Session["appLang"] = culture;
            }
            
            Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(culture);
        }
            void Application_Start(object sender, EventArgs e)
        {


        }

        void Application_End(object sender, EventArgs e)
        {
            //  Código que se ejecuta cuando se cierra la aplicación

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Código que se ejecuta al producirse un error no controlado

        }

        void Session_Start(object sender, EventArgs e)
        {
            Session["Logged"] = "No";
            Session["User"] = "";
            Session["URLMain"] = "main.aspx";

        }

        void Session_End(object sender, EventArgs e)
        {
            // Código que se ejecuta cuando finaliza una sesión. 
            // Nota: El evento Session_End se desencadena sólo con el modo sessionstate
            // se establece como InProc en el archivo Web.config. Si el modo de sesión se establece como StateServer 
            // o SQLServer, el evento no se genera.

        }
    }
}