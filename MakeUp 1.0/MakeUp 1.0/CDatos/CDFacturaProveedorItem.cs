﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

using CEntidades;
namespace CDatos
{
    [Serializable]
    public class CDFacturaProveedorItem
    {
        public void Insert(CEFacturaProveedorItem facturaProvItem)
        {
            //Creamos nuestro objeto de conexion usando nuestro archivo de configuraciones
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                string insertedID = "";
                cnx.Open();
                //Declaramos nuestra consulta de Acción Sql parametrizada
                const string sqlQuery =
                    "INSERT INTO Detalle_X_FacturaProveedor (dcfp_fac_id,dcfp_prod_id,dcfp_precio,dcfp_cantidad,dcfp_alta,dcfp_baja,dv) VALUES (@fac_prov, @produdcto_id,@precio,@cantidad,getdate(),null,@dv);SELECT SCOPE_IDENTITY()";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    //El primero de los cambios significativos con respecto al ejemplo descargado es que aqui...
                    //ya no leeremos controles sino usaremos las propiedades del Objeto EProducto de nuestra capa
                    //de entidades...
                    cmd.Parameters.AddWithValue("@fac_prov", facturaProvItem.factura_id);
                    cmd.Parameters.AddWithValue("@produdcto_id", facturaProvItem.producto_id);
                    cmd.Parameters.AddWithValue("@precio", facturaProvItem.precio_compra);
                    cmd.Parameters.AddWithValue("@cantidad", facturaProvItem.cantidad);
                    cmd.Parameters.AddWithValue("@dv", 1);

                    insertedID = cmd.ExecuteScalar().ToString();
                    //insertedID = cmd.ExecuteNonQuery().ToString();
                    cnx.Close();
                    facturaProvItem.id = Convert.ToInt32(insertedID);
                }
            }
        }

    }
}
