﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace CDatos
{
    //Clase que lista los productos del catalogo, para poder mostrar por pantalla.
    public class CDProductos { 
     //   Propiedades de la clase que obtiene o asigna los atributos privados de la misma.
    public int id { get; set; }
    public string Descripcion { get; set; }
    public int precio { get; set; }
    public int cantidad { get; set; }
    public string urlImagen { get; set; }
    public int DVH { get; set; }

    //Metodo que inserta la venta final para poder guardarla en la base de datos.
    public static void insertarVenta(CDUsuario u, CNegocio.CNCarritoCompra compra)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
        {
            foreach (ItemProducto item in compra.obtenerProductos)
            {
                string sql = @"INSERT INTO ventas (
                                               idusuario
                                              ,idproducto
                                              ,Cantidad
                                               ,fecha
                                                ,precio)
                                          VALUES (
                                                @usuario, 
                                                @producto, 
                                                @Cant,
                                                @fecha,
                                                @precio)";




                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("usuario", u.id);
                command.Parameters.AddWithValue("producto", item.id);
                command.Parameters.AddWithValue("Cant", item.cantidad);
                command.Parameters.AddWithValue("fecha", DateTime.Now);
                command.Parameters.AddWithValue("precio", item.precio);



                conn.Open();

                command.ExecuteNonQuery();
                conn.Close();

            }



        }
    }



    //Metodo que obtiene todos los productos del catalogo.
    public static DataSet obtenerProductos()
    {

        string sql = @"select * from productos";

        DataSet ds = new DataSet();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
        SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        ds.Clear();
        da.Fill(ds, "Productos");
        return ds;



    }
    //Metodo que sirva para insertar en la base de datos un nuevo producto realizaro por 
    //el administrador del sistema.
    public static Productos insertarProducto(Productos p)
    {


        string sql = @"INSERT INTO productos (
                           Descripcion
                          ,Precio
                          ,Cantidad
                          ,UrlImagen
                          ,DV)
                      VALUES (
                            @Desc, 
                            @Precio, 
                            @Cant,
                            @UrlImagen,
                            @DVH)
                    SELECT SCOPE_IDENTITY()";


        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
        {

            SqlCommand command = new SqlCommand(sql, conn);
            command.Parameters.AddWithValue("Desc", p.Descripcion);
            command.Parameters.AddWithValue("Precio", p.precio);
            command.Parameters.AddWithValue("Cant", p.cantidad);
            command.Parameters.AddWithValue("UrlImagen", p.urlImagen);
            command.Parameters.AddWithValue("DVH", 0);

            conn.Open();

            p.id = Convert.ToInt32(command.ExecuteScalar());
            //Cada ves que inserto se actualiza el digito verificador.
            DigVer.DigVer.actualizarDVH("productos", p.id);
            return p;
        }
    }
}
}