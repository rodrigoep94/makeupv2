﻿using CEntidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDatos
{
    public class CDMensajes
    {
        public CEMensaje InsertarMensaje(CEMensaje mensaje)
        {

            string sql = @"INSERT INTO mensajes (
                           msj_emisor
                          ,msj_emisor_id
                          ,msj_destinatario
                          ,msj_destinatario_id
                          ,msj_fecha
                          ,msj_texto)
                      VALUES (
                            @emisor, 
                            @emisor_id,
                            @destinatario, 
                            @destinatario_id, 
                            @fecha,
                            @texto)
                    SELECT SCOPE_IDENTITY()";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {

                SqlCommand command = new SqlCommand(sql, conn);

                if (mensaje.Emisor_Id == null)
                {
                    command.Parameters.AddWithValue("emisor_id", DBNull.Value);
                }
                else
                {
                    command.Parameters.AddWithValue("emisor_id", mensaje.Emisor_Id);
                }

                command.Parameters.AddWithValue("emisor", mensaje.Emisor);
                command.Parameters.AddWithValue("destinatario", mensaje.Destinatario);
                command.Parameters.AddWithValue("destinatario_id", mensaje.Destinatario_Id);
                command.Parameters.AddWithValue("fecha", mensaje.FechaEnvio);
                command.Parameters.AddWithValue("texto", mensaje.Mensaje);

                conn.Open();

                mensaje.Id = Convert.ToInt32(command.ExecuteScalar());
                //Cada ves que inserto se actualiza el digito verificador.
                return mensaje;
            }
        }

        public void EliminarMensaje(int idMensaje)
        {
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();
                const string sqlQuery = "DELETE FROM Mensajes WHERE id_mensaje = @id";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    cmd.Parameters.AddWithValue("@id", idMensaje);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public DataSet ObtenerMensajes(int idUsuario)
        {
            string sql = @"select * from Mensajes WHERE msj_destinatario_id = @destinatario";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            da.SelectCommand.Parameters.AddWithValue("@destinatario", idUsuario);
            ds.Clear();
            da.Fill(ds, "Mensajes");
            conn.Close();
            return ds;
        }

        public int GetCantidadMensajes(int idUsuario)
        {
            string sql = @"SELECT COUNT(*) FROM Mensajes WHERE msj_destinatario_id = @iduser";

            int mensajes = 0;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("iduser", idUsuario);
                conn.Open();

                mensajes = Convert.ToInt32(command.ExecuteScalar());
                conn.Close();
            }
            return mensajes;
        }
    }
}
