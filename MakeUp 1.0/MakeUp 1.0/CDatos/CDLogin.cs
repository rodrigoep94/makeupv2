﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using CEntidades;

namespace CDatos
{
    public class CDLogin
    {
        public List<MenuItem> GetOpcionesDeMenu(int userType)
        {
            var lista = new List<MenuItem>();
            SqlDataReader reader;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                //Obtengo los permisos en ADO.Net modo Conectado.

                conn.Open();
                string sql = "select menuxtipousuario.idtipousuario,menu.descripcion,menu.orden from menuxtipousuario,menu";
                sql += " where menuxtipousuario.idmenu = menu.id";
                sql += string.Format(" and idtipousuario = {0} order by menu.orden", userType);
                SqlCommand command = new SqlCommand(sql, conn);
                reader = command.ExecuteReader();

                //Obtengo el String de Conexion que se encuentra en el WebConfig.

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string nombre = reader.GetValue(1).ToString();
                        MenuItem menu = new MenuItem(nombre, nombre);
                        lista.Add(menu);
                    }
                }
                conn.Close();
            }

            return lista;
        }

        public int GetUsuario(string usuario)
        {
            var count = 0;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();
                string sql2 = string.Format("select count(*) from usuarios where usu_mail = '{0}'", usuario);
                SqlCommand command2 = new SqlCommand(sql2, conn);
                count = Convert.ToInt32(command2.ExecuteScalar());
                conn.Close();
            }
            return count;
        }

        public void ReiniciarContador(CEUsuario user)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();
                string sql2 = string.Format("update Usuarios set usu_intentos = 0 where usu_mail = '{0}'", user.NombreUsuario);
                SqlCommand command2 = new SqlCommand(sql2, conn);
                command2.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void UpdateIntentosUsuario(string usuario)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                string sql = string.Format("update Usuarios set usu_intentos = usu_intentos +1 where usu_mail = '{0}'", usuario);

                conn.Open();

                SqlCommand command = new SqlCommand(sql, conn);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void GetIntentosDeUsuario(string usuario, out int intentos, out int id)
        {
            intentos = 0;
            id = 0;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                string sql = string.Format("select id,usu_intentos from Usuarios where usu_mail = '{0}'", usuario);

                conn.Open();

                SqlCommand command = new SqlCommand(sql, conn);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        id = Convert.ToInt32(reader.GetValue(0));
                        intentos = Convert.ToInt32(reader.GetValue(1));
                    }
                }
                conn.Close();
            }

        }

        public CEUsuario AutenticarUsuario(string usuario, string password, string hash)
        {

            string sql = @"SELECT id,usu_nombre,usu_apellido,usu_mail,TipoUsuario
                      FROM Usuarios
                      WHERE usu_mail = @nombre AND usu_pass = @password and usu_intentos != 2";
            //inicializo el usuario en null antes de llenarlo de datos.
            CEUsuario user = null;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("@nombre", usuario);


                command.Parameters.AddWithValue("@password", hash);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //Instancio un objeto usuario y lo lleno de datos.
                        user = new CEUsuario();
                        user.id = Convert.ToInt32(reader.GetValue(0));
                        user.Nombre = reader.GetValue(1).ToString();
                        user.Apellido = reader.GetValue(2).ToString();
                        user.NombreUsuario = reader.GetValue(3).ToString();
                        user.TipoUsuario = Convert.ToInt32(reader.GetValue(4));


                    }

                }
                conn.Close();
                //En caso que el usuario sea nulo lo devuelo para que se valide la clase Login.aspx.
            }

            return user;
        }


    }
}
