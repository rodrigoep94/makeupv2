﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

using CEntidades;

namespace CDatos
{
    public class CDProveedor
    {

        public List<CEProveedores> GetListProveedores()
        {
            List<CEProveedores> retorno = new List<CEProveedores>();
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();

                const string sqlQuery = "SELECT * FROM Proveedores ORDER BY Id ASC";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        var proveedor = new CEProveedores()
                        {
                            id = Convert.ToInt32(dataReader["id"]),
                            nombre = Convert.ToString(dataReader["prov_nombre"]),
                            cuit = Convert.ToInt32(dataReader["prov_cuit"]),
                            direccion = Convert.ToString(dataReader["prov_direccion"]),
                            telefono = Convert.ToInt32(dataReader["prov_telefono"]),
                            DV = Convert.ToInt32(dataReader["dv"]),
                        };

                        retorno.Add(proveedor);
                    }
                }
            }
            return retorno;
        }

        public CEProveedores GetProveedorById(int id)
        {
            var proveedor = new CEProveedores();
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();
                const string sqlQuery = "SELECT * FROM Proveedores WHERE Id = @prov_id ORDER BY Id ASC";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    cmd.Parameters.AddWithValue("@prov_id", id);
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        proveedor = new CEProveedores()
                        {
                            id = Convert.ToInt32(dataReader["id"]),
                            nombre = Convert.ToString(dataReader["prov_nombre"]),
                            cuit = Convert.ToInt32(dataReader["prov_cuit"]),
                            direccion = Convert.ToString(dataReader["prov_direccion"]),
                            telefono = Convert.ToInt32(dataReader["prov_telefono"]),
                            DV = Convert.ToInt32(dataReader["dv"]),
                        };
                    }
                }
                cnx.Close();
            }
            return proveedor;

        }

        public List<string>Get_Proveedores_All()
        {
            //Aqui llamas a la base de datos con la forma que mas te guste
            List<string> retorno = new List<string>();
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();

                const string sqlQuery = "SELECT prov_nombre FROM Proveedores ORDER BY Id ASC";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    //
                    //Preguntamos si el DataReader fue devuelto con datos
                    while (dataReader.Read())
                    {
                        //Insertamos el objeto Producto dentro de la lista Productos
                        retorno.Add(Convert.ToString(dataReader["Id"]));
                    }
                }
            }


            return retorno; // devuelve los valores
        }
        public CEProveedores Insert(CEProveedores proveedor)
        {
            //Creamos nuestro objeto de conexion usando nuestro archivo de configuraciones
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                string insertedID = "";
                cnx.Open();
                //Declaramos nuestra consulta de Acción Sql parametrizada
                const string sqlQuery =
                    "INSERT INTO Proveedores(prov_nombre," +
                    "                        prov_direccion," +
                    "                        prov_telefono,"+
                    "                        prov_cuit,"+
                    "                        prov_alta," +
                    "                        prov_baja," +
                    "                        DV) VALUES (@prov_nombre,@prov_direccion,@prov_telefono,@prov_cuit,getdate(),null,@dv);SELECT SCOPE_IDENTITY()";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    //El primero de los cambios significativos con respecto al ejemplo descargado es que aqui...
                    //ya no leeremos controles sino usaremos las propiedades del Objeto EProducto de nuestra capa
                    //de entidades...
                    cmd.Parameters.AddWithValue("@prov_nombre", proveedor.nombre);
                    cmd.Parameters.AddWithValue("@prov_direccion", proveedor.direccion);
                    cmd.Parameters.AddWithValue("@prov_telefono", proveedor.telefono);
                    cmd.Parameters.AddWithValue("@prov_cuit", proveedor.cuit);
                    //cmd.Parameters.AddWithValue("@dv", facturaProv.DV);
                    cmd.Parameters.AddWithValue("@dv", 1);

                    insertedID = cmd.ExecuteScalar().ToString();
                    //insertedID = cmd.ExecuteNonQuery().ToString();
                    cnx.Close();
                    proveedor.id = Convert.ToInt32(insertedID);
                }

                return proveedor;
            }
        }



    }
}
