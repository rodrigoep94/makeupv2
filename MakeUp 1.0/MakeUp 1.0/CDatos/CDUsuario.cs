﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CEntidades;

namespace CDatos
{
    public class CDUsuario
    {
        public CEUsuario insertUsuario(CEUsuario usuario, string password)
        {
            string sql = @"INSERT INTO Usuarios (id
                          ,usu_nombre
                          ,usu_apellido
                          ,usu_mail
                          ,usu_pass
                          ,TipoUsuario
                          ,usu_intentos
                          ,DV)
                      VALUES (
                            (SELECT MAX(id) + 1 FROM Usuarios),
                            @Nombre, 
                            @Apellido, 
                            @Usuario,
                            @Password,
                            @TipoUsuario,0,0)
                    SELECT MAX(id) FROM Usuarios";
            //Obtengo el string de conexion que se encuentra en el WebConfig
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("Nombre", usuario.Nombre);
                command.Parameters.AddWithValue("Apellido", usuario.Apellido);
                command.Parameters.AddWithValue("Usuario", usuario.NombreUsuario);
                command.Parameters.AddWithValue("TipoUsuario", usuario.TipoUsuario);

                //Concateno el usuario y contraseña para genera un nivel de seguridad mas alto y lo encripto 
                //con el algoritmo MDG para guardarlo en la Base de datos.
                command.Parameters.AddWithValue("Password", password);

                conn.Open();

                //  tengo el id del usuario luego de insertarlo para actualizar el DVH.
                usuario.id = Convert.ToInt32(command.ExecuteScalar());
                //Cierro la conexion.
                conn.Close();
                //Devuelo el usuario creado con el id.
                return usuario;
            }
        }

        public CEUsuario GetUser(string userName)
        {
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();

                string sqlGetById = $@"SELECT * FROM usuarios WHERE usu_mail = '{userName}'";
                using (SqlCommand cmd = new SqlCommand(sqlGetById, cnx))
                {
                    //
                    //Utilizamos el valor del parámetro idProducto para enviarlo al parámetro declarado en la consulta
                    //de selección SQL
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    if (dataReader.Read())
                    {
                        CEUsuario usuario = new CEUsuario
                        {
                            id = Convert.ToInt32(dataReader["id"]),
                            Nombre = Convert.ToString(dataReader["usu_nombre"]),
                            Apellido = Convert.ToString(dataReader["usu_apellido"]),
                            TipoUsuario = Convert.ToInt32(dataReader["TipoUsuario"]),
                            NombreUsuario = Convert.ToString(dataReader["usu_mail"])
                        };
                        cnx.Close();
                        return usuario;
                    }
                }
            }

            return null;
        }

        public DataTable GetUsers()
        {
            string sql = @"select * from usuarios";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Usuarios");
            conn.Close();
            return ds.Tables["Usuarios"];
        }

        public DataTable getUserTypesDatatable()
        {
            string sql = @"select * from tipousuarios";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "UserTypes");
            conn.Close();
            return ds.Tables["UserTypes"];
        }
    }
}
