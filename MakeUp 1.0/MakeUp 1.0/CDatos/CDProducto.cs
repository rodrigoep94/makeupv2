﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using CEntidades;

namespace CDatos
{
    public class CDProducto
    {
        public List<CEProducto> ProductosStockMinimo()
        {
            //Declaramos una lista del objeto CEProducto la cual será la encargada de
            //regresar una colección de los elementos que se obtengan de la BD
            //
            //La lista substituye a DataTable utilizado en el proyecto de ejemplo
            List<CEProducto> productosStockMinimo = new List<CEProducto>();

            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["cnnString"].ToString()))
            {
                cnx.Open();

                const string sqlQuery = "select id,pro_Nombre from productos where pro_Cantidad  <= pro_stockMinimo";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    //
                    //Preguntamos si el DataReader fue devuelto con datos
                    while (dataReader.Read())
                    {
                        //
                        //Instanciamos al objeto Eproducto para llenar sus propiedades
                        CEProducto Listaproductos = new CEProducto
                        {
                            id = Convert.ToInt32(dataReader["id"]),
                            nombre = Convert.ToString(dataReader["pro_Nombre"]),
                        };
                        //
                        //Insertamos el objeto Producto dentro de la lista Productos
                        productosStockMinimo.Add(Listaproductos);
                    }
                }

                cnx.Close();
            }
            return productosStockMinimo;
        }

        public void UpdateStock(int producto_id, int cantidad)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();
                string sql = $@"UPDATE Productos SET pro_Cantidad = pro_Cantidad + {cantidad} WHERE id = {producto_id}";

                //Obtengo el string de conexion que se encuentra en el WebConfig
                SqlCommand command = new SqlCommand(sql, conn);
                command.ExecuteNonQuery();
                conn.Close();
            };
        }

        public void DisminuirStockProductos(List<CEntidades.CEItemProducto> productos)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();
                productos.ForEach(p =>
                {
                    string sql = $@"UPDATE Productos SET pro_Cantidad = pro_Cantidad - {p.cantidad} WHERE id = {p.id}";

                    //Obtengo el string de conexion que se encuentra en el WebConfig
                    SqlCommand command = new SqlCommand(sql, conn);
                    command.ExecuteNonQuery();

                    //Cierro la conexion.
                });
                conn.Close();
            };
        }

        public bool HayStockDisponible(int idProd)
        {
            var producto = this.ObtenerProducto(idProd);
            return producto.stock > producto.cantidad;
        }

        public CEProducto ObtenerProducto(int idProd)
        {
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();

                const string sqlGetById = "SELECT * FROM Productos WHERE Id = @id";
                using (SqlCommand cmd = new SqlCommand(sqlGetById, cnx))
                {
                    //
                    //Utilizamos el valor del parámetro idProducto para enviarlo al parámetro declarado en la consulta
                    //de selección SQL
                    cmd.Parameters.AddWithValue("@id", idProd);
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    if (dataReader.Read())
                    {
                        CEProducto producto = new CEProducto
                        {
                            id = Convert.ToInt32(dataReader["id"]),
                            cantidad = Convert.ToInt32(dataReader["pro_StockMinimo"]),
                            Descripcion = Convert.ToString(dataReader["pro_Nombre"]),
                            nombre = Convert.ToString(dataReader["pro_Nombre"]),
                            precio = Convert.ToInt32(dataReader["pro_Precio"]),
                            stock = Convert.ToInt32(dataReader["pro_cantidad"]),
                            urlImagen = Convert.ToString(dataReader["pro_UrlImagen"])
                        };

                        return producto;
                    }
                }
            }

            return null;
        }

        //Metodo que utilizo para obtener los filtros de bitacora
        public static DataTable obtenerFiltro()
        {
            string sql = string.Format("select distinct(pro_Nombre) from Productos order by 1 desc");
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Filtro");
            conn.Close();
            return ds.Tables["Filtro"];
        }

        public CEProducto insertarProducto(CEProducto p)
        {
            string sql = @"INSERT INTO productos (
                           pro_Nombre
                          ,pro_Precio
                          ,pro_Cantidad
                          ,pro_UrlImagen
                          ,pro_alta
                          ,pro_StockMinimo
                          ,DV)
                      VALUES (
                            @Desc, 
                            @Precio, 
                            @stock,
                            @UrlImagen,
                            getdate(),
                            @Cant,
                            @DVH)
                    SELECT SCOPE_IDENTITY()";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("Desc", p.Descripcion);
                command.Parameters.AddWithValue("Precio", p.precio);
                command.Parameters.AddWithValue("Cant", p.cantidad);
                command.Parameters.AddWithValue("UrlImagen", p.urlImagen);
                command.Parameters.AddWithValue("stock", p.stock);
                command.Parameters.AddWithValue("DVH", 0);

                conn.Open();

                p.id = Convert.ToInt32(command.ExecuteScalar());
                //Cada ves que inserto se actualiza el digito verificador.
                return p;
            }
        }

        public DataSet obtenerProductos()
        {
            string sql = @"select * from productos";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Productos");
            conn.Close();
            return ds;
        }
    }
}
