﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDatos
{
    public class CDDigVer
    {
        public DataTable GetTable()
        {
            //Obtengo todas las tablas que hay que comprobar los DVH y DVV.
            string queryTable = "select * from digver";
            DataTable table = new DataTable("DVVertical");

            //Me conecto a la base de datos en modo desconectado y lleno un datable para ir recorriendo los resultados.
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(queryTable, conn);
            table.Clear();
            da.Fill(table);
            conn.Close();
            return table;
        }

        public void ActualizarDVVTabla(string tablename)
        {
            int DVV = 0;
            //Obtengo la suma total de los DVV 
            string querySuma = string.Format("SELECT ISNULL(SUM(dv), 0) FROM {0}", tablename);
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {

                SqlCommand command = new SqlCommand(querySuma, conn);

                conn.Open();

                DVV = Convert.ToInt32(command.ExecuteScalar());
                //Actualizo el dvv en la tabla DIGVER
                string update = string.Format("update digver set dvv = {0} where tabla = '{1}'", DVV, tablename);
                SqlCommand commandUpdate = new SqlCommand(update, conn);

                commandUpdate.ExecuteNonQuery();
                conn.Close();
            }
        }

        public DataTable GetValorDeTabla(string tablename, int id)
        {
            string queryTable = string.Format("select * from {0} where id = {1}", tablename, id);
            DataTable table = new DataTable(tablename);
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(queryTable, conn);
            table.Clear();
            da.Fill(table);
            conn.Close();
            return table;
        }

        public void UpdateDV(string tablename, int resultH, int id)
        {
            //Realizo el update del DVV
            string update = string.Format("update {0} set DV = {1} where id = {2}", tablename, resultH, id);
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlCommand command = new SqlCommand(update, conn);
            conn.Open();
            command.ExecuteNonQuery();
            conn.Close();
        }

        public DataTable GetValoresDeTabla(string tablename)
        {
            string queryTable = string.Format("select * from {0}", tablename);
            DataTable table = new DataTable(tablename);
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(queryTable, conn);
            table.Clear();
            da.Fill(table);
            return table;
        }
    }
}
