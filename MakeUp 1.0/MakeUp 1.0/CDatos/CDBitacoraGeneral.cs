﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDatos
{
    public class CDBitacoraGeneral
    {
        public void InsertarBitacora(string mensaje, string user)
        {
            string sql = @"INSERT INTO bitacora (
                           bit_usu,
                           bit_fun,
                           bit_fecha,
                           bit_criticidad                          
                          )
                      VALUES (
                             
                            @usuario,
                            @desc, 
                            getDate(),
                            @criticidad)";


            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {


                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("desc", mensaje);
                command.Parameters.AddWithValue("usuario", user);
                command.Parameters.AddWithValue("criticidad", "Alta");

                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();

            }
        }

        //Metodo que se conecta a la base de datos y devuelve todos los items de bitacora
        //que muestra los eventos mas cercanos primeros en la lista.
        public static DataTable obtenerBitacora()
        {
            string sql = @"select * from bitacora order by 1 desc";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Bitacora");
            return ds.Tables["Bitacora"];
        }

        //Metodo que utilizo para obtener los filtros de bitacora
        public static DataTable obtenerFiltro(string tabla, string filtro)
        {
            string sql = string.Format("select distinct({0}) from {1} order by 1 desc", filtro, tabla);
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Filtro");
            return ds.Tables["Filtro"];
        }

        //Metodo que utilizo para obtener el resultado de la busqueda en bitacora con los filtros, 
        //dependiendo lo que se envie es lo que devuelvo
        public static DataTable obtenerResultadoBusqueda(string filtro1, string filtro2, string filtro3, string fechaDesde, string fechaHasta)
        {
            string sql = string.Format(@"SELECT * 
                                        FROM bitacora 
                                        WHERE bit_fecha >='{0}' 
                                                AND bit_fecha <='{1}' 
                                                AND bit_usu LIKE '%{2}%'
                                                AND bit_criticidad LIKE '%{3}%'
                                                AND bit_fun LIKE '%{4}%'
                                        ORDER BY 1 DESC",
                                        fechaDesde,
                                        fechaHasta,
                                        filtro1,
                                        filtro2,
                                        filtro3);

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Filtro");
            return ds.Tables["Filtro"];
        }
    }
}
