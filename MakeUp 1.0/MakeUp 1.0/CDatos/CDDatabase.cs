﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDatos
{
    public class CDDatabase
    {
        public void Backup(string nombre, string message)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {

                string AppPath = @"C:\";
                AppPath += @"Backup\" + nombre;
                string query = "BACKUP DATABASE [MAKEUP]";
                query += string.Format(" TO  DISK = N'{0}'", AppPath);
                query += " WITH NOFORMAT, NOINIT,  ";
                query += string.Format(" NAME = N'{0}', ", nombre);
                query += "SKIP, NOREWIND, NOUNLOAD,  STATS = 10";


                conn.Open();
                SqlCommand command = new SqlCommand(query, conn);
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public bool Restore(string archivo, string message)
        {
            bool generarRestore = false;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["restore"].ToString());
            String nombreBase = "LPPA2";
            String ubicacionBAK = System.AppDomain.CurrentDomain.BaseDirectory + @"\Backup\";
            string query = "EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'" + nombreBase + "' ; ALTER DATABASE " + nombreBase + " SET  SINGLE_USER WITH ROLLBACK IMMEDIATE; RESTORE DATABASE " + nombreBase + " FROM  DISK = N'" + ubicacionBAK + archivo + "' WITH RESTRICTED_USER, FILE = 1,  NOUNLOAD,  REPLACE,  STATS = 10;ALTER DATABASE " + nombreBase + " SET  MULTI_USER WITH NO_WAIT";
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = connection;
            cmd.Connection.Open();
            try
            {
                cmd.ExecuteNonQuery();
                generarRestore = true;
                return generarRestore;
            }
            catch (Exception ex)
            {

                message = ex.ToString();
            }
            finally
            {
                cmd.Connection.Close();

            }

            return generarRestore;
        }
    }
}
