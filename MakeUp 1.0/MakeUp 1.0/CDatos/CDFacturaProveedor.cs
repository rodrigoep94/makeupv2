﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

using CEntidades;
using System.Data;

namespace CDatos
{
    public class CDFacturaProveedor
    {
        public CEFacturaProveedor Insert(CEFacturaProveedor facturaProv)
        {
            //Creamos nuestro objeto de conexion usando nuestro archivo de configuraciones
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                string insertedID = "";
                cnx.Open();
                //Declaramos nuestra consulta de Acción Sql parametrizada
                const string sqlQuery =
                    "INSERT INTO Facturas (fac_prov,fac_est,fac_numero,fac_alta,fac_baja,DV) VALUES (@fac_prov, 1, @fact_numero,getdate(),null,@dv);SELECT SCOPE_IDENTITY()";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    //El primero de los cambios significativos con respecto al ejemplo descargado es que aqui...
                    //ya no leeremos controles sino usaremos las propiedades del Objeto EProducto de nuestra capa
                    //de entidades...
                    cmd.Parameters.AddWithValue("@fac_prov", facturaProv.Id_fac_prov);
                    cmd.Parameters.AddWithValue("@fact_numero", facturaProv.fac_numero);
                    //cmd.Parameters.AddWithValue("@dv", facturaProv.DV);
                    cmd.Parameters.AddWithValue("@dv", 1);

                    insertedID = cmd.ExecuteScalar().ToString();
                    //insertedID = cmd.ExecuteNonQuery().ToString();
                    cnx.Close();
                    facturaProv.Id = Convert.ToInt32(insertedID);
                }
                return facturaProv;
            }
        }
        /// <summary>
        /// Devuelve una lista de Productos ordenados por el campo Id de manera Ascendente
        /// </summary>
        /// <returns>Lista de productos</returns>
        /// 
        public static DataTable obtenerProveedores()
        {
            string sql = string.Format("select id,prov_nombre from Proveedores order by 1 desc");
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Filtro");
            return ds.Tables["Filtro"];
        }

        public List<CEEstadoFactura> GetEstadosFactura()
        {
            List<CEEstadoFactura> retorno = new List<CEEstadoFactura>();
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();

                const string sqlQuery = "SELECT * FROM Estados ORDER BY Id ASC";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        var proveedor = new CEEstadoFactura()
                        {
                            Id = Convert.ToInt32(dataReader["id"]),
                            Nombre = Convert.ToString(dataReader["fac_nombre"])
                        };

                        retorno.Add(proveedor);
                    }
                }
            }
            return retorno;
        }

        public List<CEFacturaProveedor> GetAll()
        {
            //Declaramos una lista del objeto EProducto la cual será la encargada de
            //regresar una colección de los elementos que se obtengan de la BD
            //
            //La lista substituye a DataTable utilizado en el proyecto de ejemplo
            List<CEFacturaProveedor> listafacturaproveedor = new List<CEFacturaProveedor>();

            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();

                const string sqlQuery = "SELECT * FROM Productos ORDER BY Id ASC";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    //
                    //Preguntamos si el DataReader fue devuelto con datos
                    while (dataReader.Read())
                    {
                        //
                        //Instanciamos al objeto Eproducto para llenar sus propiedades
                        CEFacturaProveedor facturaproveedor = new CEFacturaProveedor
                        {
                            Id = Convert.ToInt32(dataReader["Id"])
                            //Descripcion = Convert.ToString(dataReader["Descripcion"]),
                            //Marca = Convert.ToString(dataReader["Marca"]),
                            //Precio = Convert.ToDecimal(dataReader["Precio"])

                        };
                        //
                        //Insertamos el objeto Producto dentro de la lista Productos
                        listafacturaproveedor.Add(facturaproveedor);
                    }
                }
            }
            return listafacturaproveedor;
        }

        /// <summary>
        /// Devuelve un Objeto Producto
        /// </summary>
        /// <param name="idProducto">Id del producto a buscar</param>
        /// <returns>Un registro con los valores del Producto</returns>
        public CEFacturaProveedor GetByid(int idfacturaproveedor)
        {
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();

                const string sqlGetById = "SELECT * FROM Producto WHERE Id = @id";
                using (SqlCommand cmd = new SqlCommand(sqlGetById, cnx))
                {
                    //
                    //Utilizamos el valor del parámetro idProducto para enviarlo al parámetro declarado en la consulta
                    //de selección SQL
                    cmd.Parameters.AddWithValue("@id", idfacturaproveedor);
                    SqlDataReader dataReader = cmd.ExecuteReader();
                    if (dataReader.Read())
                    {
                        CEFacturaProveedor producto = new CEFacturaProveedor
                        {
                            Id = Convert.ToInt32(dataReader["Id"])
                            //Descripcion = Convert.ToString(dataReader["Descripcion"]),
                            //Marca = Convert.ToString(dataReader["Marca"]),
                            //Precio = Convert.ToDecimal(dataReader["Precio"])
                        };

                        return producto;
                    }
                }
            }

            return null;
        }
        /// <summary>
        /// Elimina un registro coincidente con el Id Proporcionado
        /// </summary>
        /// <param name="idproducto">Id del registro a Eliminar</param>
        public void Delete(int idfacturaproveedor)
        {
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();
                const string sqlQuery = "DELETE FROM Producto WHERE Id = @id";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    cmd.Parameters.AddWithValue("@id", idfacturaproveedor);

                    cmd.ExecuteNonQuery();
                }
            }
        }

    }

}