﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CDatos
{
    public class CDNotificaciones
    {
        public int GetCantidadNotificaciones()
        {
            string sql = @"SELECT COUNT(*) FROM Notificaciones";
            int notificaciones = 0;
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {

                SqlCommand command = new SqlCommand(sql, conn);

                conn.Open();

                notificaciones = Convert.ToInt32(command.ExecuteScalar());
                conn.Close();
            }
            return notificaciones;
        }

        public DataSet ObtenerNotificaciones()
        {
            string sql = @"select * from Notificaciones";

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            ds.Clear();
            da.Fill(ds, "Notificaciones");
            conn.Close();
            return ds;
        }

        public void InsertarNotificacion(int idProd, string mensaje)
        {
            string sql = @"INSERT INTO Notificaciones    (
                           id_notificacion
                          ,id_producto
                          ,mensaje)
                      VALUES (
                            (SELECT ISNULL(MAX(id_notificacion), 0) + 1 FROM Notificaciones), 
                            @id, 
                            @mensaje)
                    SELECT SCOPE_IDENTITY()";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("id", idProd);
                command.Parameters.AddWithValue("mensaje", mensaje);

                conn.Open();
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public void EliminarNotificacion(int idNotificacion)
        {
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                cnx.Open();
                const string sqlQuery = "DELETE FROM Notificaciones WHERE id_notificacion = @id";
                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    cmd.Parameters.AddWithValue("@id", idNotificacion);

                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
