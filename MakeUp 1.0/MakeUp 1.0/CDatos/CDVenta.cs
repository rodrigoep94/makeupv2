﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using CEntidades;
using System.Data;

namespace CDatos
{
    public class CDVenta
    {

        public int InsertarVenta(int userId)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                string sql = @"INSERT INTO Ventas (
                                               id_venta
                                              ,id_usuario
                                              ,fecha_venta
                                               ,dvh
                                                ,dvv)
                                          VALUES (
                                                (SELECT ISNULL(MAX(id_venta), 0) + 1 FROM ventas),
                                                @usuario,
                                                @fecha,
                                                0,
                                                0);
												SELECT MAX(id_venta) FROM Ventas WHERE id_usuario = @usuario AND fecha_venta = @fecha";

                SqlCommand command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("@usuario", userId);
                command.Parameters.AddWithValue("@fecha", DateTime.Now);

                conn.Open();

                var insertedID = Convert.ToInt32(command.ExecuteScalar());
                conn.Close();

                return insertedID;
            }
        }

        public void InsertarProductosDeVenta(int idVenta, List<CEItemProducto> productos)
        {

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                foreach (CEItemProducto item in productos)
                {
                    string sql = @"INSERT INTO Producto_X_Venta (
                                               id_venta
                                              ,id_producto
                                              ,cantidad
                                              ,precio)
                                          VALUES (
                                                @id_venta,
                                                @id_producto, 
                                                @cantidad,
                                                @precio)";

                    SqlCommand command = new SqlCommand(sql, conn);
                    command.Parameters.AddWithValue("@id_producto", item.id);
                    command.Parameters.AddWithValue("@cantidad", item.cantidad);
                    command.Parameters.AddWithValue("@id_venta", idVenta);
                    command.Parameters.AddWithValue("@precio", item.precio);

                    conn.Open();

                    command.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        public DataSet ObtenerDatosDeVenta(int userId)
        {

            string sql = @"SELECT id_venta, fecha_venta,
                                (SELECT pro_Nombre + ' x ' + CAST(cantidad AS VARCHAR) + ': $' + CAST(precio * cantidad AS VARCHAR) + '; '
                                FROM Producto_X_Venta JOIN Productos ON Producto_X_Venta.id_producto = Productos.id 
                                WHERE id_venta = Ventas.id_venta
                                FOR XML PATH('')) As Detalle
                            FROM Ventas
                            WHERE id_usuario = @id";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString());

            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@id", userId);

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                DataSet ds = new DataSet();
                ds.Clear();
                da.Fill(ds, "Ventas");
                return ds;
            }
        }
    }

}