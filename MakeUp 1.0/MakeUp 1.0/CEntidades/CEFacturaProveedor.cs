﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEntidades
{
    public class CEFacturaProveedor
    {
        public int Id { get; set; }
        public int Id_fac_prov { get; set; }
        public int Id_fac_est { get; set; }
        public string fac_numero { get; set; }
        public DateTime fac_fecha_compra { get; set; }
        public DateTime fac_alta { get; set; }
        public DateTime fac_baja { get; set; }
        public int DV { get; set; }

    }
}