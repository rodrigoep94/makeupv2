﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEntidades
{
    public class CEMensaje
    {
        public int Id { get; set; }
        public string Mensaje { get; set; }
        public int? Emisor_Id { get; set; }
        public string Emisor { get; set; }
        public int Destinatario_Id { get; set; }
        public string Destinatario { get; set; }
        public DateTime FechaEnvio { get; set; }
    }
}
