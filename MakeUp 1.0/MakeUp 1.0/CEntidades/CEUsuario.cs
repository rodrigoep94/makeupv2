﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEntidades
{
    public class CEUsuario
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public string NombreUsuario { get; set; }
        public string Password { get; set; }
        public int TipoUsuario { get; set; }
    }
}
