﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEntidades
{
    public class CEItemProducto
    {
        public CEItemProducto(int id, string nombre, int precio, string imagen, int cantidadMinima, int stock)
        {
            this.id = id;
            this.nombre = nombre;
            this.precio = precio;
            this.imagen = imagen;
            this.cantidadMinima = cantidadMinima;
            this.stock = stock;
        }

        public int id { get; set; }

        public int subtotal { get; set; }

        public int cantidad { get; set; }

        public int precio { get; set; }

        public int cantidadMinima { get; set; }

        public int stock { get; set; }

        public string nombre { get; set; }

        public string imagen { get; set; }
    }
}
