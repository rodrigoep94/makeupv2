﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEntidades
{
    [Serializable]
    public class CEProducto
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int DV { get; set; }
        public int precio { get; set; }
        public int cantidad { get; set; }
        public string urlImagen { get; set; }
        public int DVH { get; set; }
        public string Descripcion { get; set; }
        public int stock { get; set; }
    }
}
