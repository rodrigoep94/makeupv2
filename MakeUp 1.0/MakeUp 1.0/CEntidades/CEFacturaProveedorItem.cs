﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEntidades
{
    [Serializable]
    public class CEFacturaProveedorItem
    {
        public int id { get; set; }
        public int factura_id { get; set; }
        public int producto_id { get; set; }
        public int precio_compra { get; set; }
        public int cantidad { get; set; }
        public DateTime Fecha_alta { get; set; }
        public DateTime Fecha_baja { get; set; }
        public int dv { get; set; }
        public string producto_nombre { get; set; }

    }
}
