﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEntidades
{
    public class CEProveedores
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public int cuit { get; set; }
        public int telefono { get; set; }
        public int DV { get; set; }
    }

}
