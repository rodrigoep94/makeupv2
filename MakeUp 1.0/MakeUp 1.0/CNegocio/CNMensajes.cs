﻿using CDatos;
using CEntidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNegocio
{
    public class CNMensajes
    {
        private CDMensajes _mensajesDal = new CDMensajes();

        public CEMensaje InsertarMensaje(CEMensaje mensaje)
        {
            return _mensajesDal.InsertarMensaje(mensaje);
        }

        public int GetCantidadMensajes(int idUsuario)
        {
            var mensajes = _mensajesDal.GetCantidadMensajes(idUsuario);

            return mensajes;
        }
        public DataSet ObtenerMensajes(int idUsuario)
        {
            var ds = _mensajesDal.ObtenerMensajes(idUsuario);
            return ds;
        }

        public void EliminarMensaje(int idMensaje)
        {
            _mensajesDal.EliminarMensaje(idMensaje);
        }
    }
}
