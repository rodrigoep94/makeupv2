﻿using CDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNegocio
{
    public class CNNotificaciones
    {
        private CDNotificaciones _CDNotificacionesDal = new CDNotificaciones();

        public DataSet ObtenerNotificaciones()
        {
            var ds = _CDNotificacionesDal.ObtenerNotificaciones();
            return ds;
        }

        public void EliminarNotificacion(int idNotificacion)
        {
            _CDNotificacionesDal.EliminarNotificacion(idNotificacion);
        }

        //Metodo para realizar altas de usuario
        public int GetCantidadNotificaciones()
        {
            var notificaciones = _CDNotificacionesDal.GetCantidadNotificaciones();

            return notificaciones;
        }
    }
}
