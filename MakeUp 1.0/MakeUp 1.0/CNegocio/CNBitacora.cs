﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CDatos;
namespace CNegocio
{
    //Clase que maneja la bitacora, y contiene el patron state para verificar el tipo
    //de registro que se va a insertar en bitacora. 
    public class CNBitacora
    {
        private IState state;
        //Metodo que inserta en bitacora en caso de que haya un error de 
        //Digito verificador vertical. Se convierte la clase en un tipo de ErrorDigVV.
        //en la cual se pasa por parametro la tabla en la que ocurrio el evento.
        public void ErrorDVVState(string tableName)
        {
            state = new ErrorDigVV(tableName);
            state.insertBitacora();
        }

        public DataTable obtenerFiltro(string v1, string v2)
        {
            return CDBitacoraGeneral.obtenerFiltro(v1, v2);
        }

        //Metodo que inserta en bitacora en caso de que haya un error de 
        //Digito verificador Horizontal. Se convierte la clase en un tipo de ErrorDigVH.
        //en la cual se para por parametro la tabla y el registro en donde ocurrio el evento.
        public void ErrorDVHState(string tableName, int idReg)
        {
            state = new ErrorDigVH(tableName, idReg);
            state.insertBitacora();
        }
        //Metodo que inserta en bitacora en caso de que haya suceda un
        //fallo de contraseña. Se conviernte la clase a un tipo de FalloContraseña.
        //en la cual se pasa por parametro el usuario que produjo el evento.
        public void ErrorFalloContraseñaState(string Usuario)
        {
            state = new FalloContraseña(Usuario);
            state.insertBitacora();
        }
        //Metodo que inserta en bitacora en caso de que haya suceda un
        //bloqueo de usuario. Se conviernte la clase a un tipo de UsuarioBloqueado.
        //en la cual se pasa por parametro el usuario que produjo el evento.
        public void ErrorBloqueoUsuarioState(string Usuario)
        {
            state = new UsuarioBloqueado(Usuario);
            state.insertBitacora();
        }
        
        public void GuardarBitacora(string mensaje, string usuario)
        {
            var bitacoraBusiness = new CDBitacoraGeneral();
            bitacoraBusiness.InsertarBitacora(mensaje, usuario);
        }

        public static DataTable obtenerBitacora()
        {
            return CDBitacoraGeneral.obtenerBitacora();
        }

        public DataTable obtenerResultadoBusqueda(string selectedValue1, string selectedValue2, string selectedValue3, string dateTime1, string dateTime2)
        {
            return CDBitacoraGeneral.obtenerResultadoBusqueda(selectedValue1, selectedValue2, selectedValue3, dateTime1, dateTime2);
        }
    }
}