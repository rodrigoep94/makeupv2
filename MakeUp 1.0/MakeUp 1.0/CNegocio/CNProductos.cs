﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using CDatos;
using CEntidades;

namespace CNegocio
{
    //Clase que lista los productos del catalogo, para poder mostrar por pantalla.
    public class CNProductos
    {
        //Instanciamos nuestra clase facturaProveedorDal para poder utilizar sus miembros
        private CDProducto _CDProductosDal = new CDProducto();
        private CDVenta _CDVentasDal = new CDVenta();
        private CDNotificaciones _CDNotificacionesDal = new CDNotificaciones();

        //Metodo que inserta la venta final para poder guardarla en la base de datos.
        public void insertarVenta(CEUsuario u, CNCarritoCompra compra)
        {
            var idVenta = this._CDVentasDal.InsertarVenta(u.id);
            var productos = compra.obtenerProductos;
            this._CDVentasDal.InsertarProductosDeVenta(idVenta, productos);
            this._CDProductosDal.DisminuirStockProductos(productos);
        }

        //Metodo que obtiene todos los productos del catalogo.
        public DataSet obtenerProductos()
        {
            var ds = _CDProductosDal.obtenerProductos();
            return ds;
        }

        //Metodo que sirva para insertar en la base de datos un nuevo producto realizaro por 
        //el administrador del sistema.
        public CEProducto insertarProducto(CEProducto p)
        {
            var producto = _CDProductosDal.insertarProducto(p);
            var digVerNeg = new CNDigVer();
            digVerNeg.actualizarDVH("productos", producto.id);
            return producto;
        }

        //Metodo que utilizo para obtener los filtros de bitacora
        public static DataTable obtenerFiltroProducto()
        {
            DataSet ds = new DataSet();
            return null;
        }

        public void NotificarFaltaStock(int idProd)
        {
            var producto = _CDProductosDal.ObtenerProducto(idProd);
            _CDNotificacionesDal.InsertarNotificacion(idProd, $"No hay stock minimo suficiente para el producto {producto.nombre}");
        }

        public bool HayStockDisponible(int idProd)
        {
            var stock = _CDProductosDal.HayStockDisponible(idProd);
            return stock;
        }
    }

}