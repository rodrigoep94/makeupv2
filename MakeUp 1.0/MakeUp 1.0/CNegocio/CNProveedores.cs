﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDatos;
using CEntidades;

namespace CNegocio
{
    public class CNProveedores
    {
        //Instanciamos nuestra clase ProveedorDal para poder utilizar sus miembros
        private CDProveedor _proveedorDal = new CDProveedor();
        //

        public List<string> Get_Proveedores_All()
        {
            CDProveedor Proveedores_All = new CDProveedor();
            return Proveedores_All.Get_Proveedores_All();
        }

        public CEProveedores Registrar(CEProveedores proveedor)
        {
            var proveedorReturn = _proveedorDal.Insert(proveedor);
            var digVerNeg = new CNDigVer();
            digVerNeg.actualizarDVH("Proveedores", proveedor.id);
            return proveedorReturn;
        }

        public List<CEProveedores> GetProveedores()
        {
            return _proveedorDal.GetListProveedores();
        }

        public CEProveedores GetProveedorById(int id)
        {
            return _proveedorDal.GetProveedorById(id);
        }
    }
}
