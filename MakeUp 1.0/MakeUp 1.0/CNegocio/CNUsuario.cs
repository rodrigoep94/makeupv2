﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using CEntidades;
using CDatos;

namespace CNegocio
{
    public class CNUsuario
    {
        private CDUsuario _CDUsuariosDal = new CDUsuario();
        //Metodo para realizar altas de usuario
        public CEUsuario Insert(CEUsuario usuario)
        {
            var password = CNEncriptar.GetMD5(string.Concat(usuario.NombreUsuario, usuario.Password));
            var usuarioInsertado = _CDUsuariosDal.insertUsuario(usuario, password);

            //Actualizo el DVH y DVV.
            var digVerNeg = new CNDigVer();
            digVerNeg.actualizarDVH("usuarios", usuarioInsertado.id);

            return usuarioInsertado;
        }

        public CEUsuario GetUserByName(string userName)
        {
            return this._CDUsuariosDal.GetUser(userName);
        }

        //Obtiene los tipos de usuarios existentes. (Webmaste,Administrador,Cliente).
        public DataTable GetUserTypes()
        {
            return _CDUsuariosDal.getUserTypesDatatable();
        }
        
        public DataTable GetUsers()
        {
            return _CDUsuariosDal.GetUsers();
        }
    }
}
