﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using CEntidades;
using CDatos;
namespace CNegocio
{
    public class CNFacturaProveedor
    {
        //Instanciamos nuestra clase facturaProveedorDal para poder utilizar sus miembros
        private CDFacturaProveedor _facturaproveedorDal = new CDFacturaProveedor();
        //
        //El uso de la clase StringBuilder nos ayudara a devolver los mensajes de las validaciones
        public readonly StringBuilder stringBuilder = new StringBuilder();

        //
        //Creamos nuestro método para Insertar un nuevo Producto, observe como este método tampoco valida los el contenido
        //de las propiedades, sino que manda a llamar a una Función que tiene como tarea única hacer esta validación
        //
        public CEFacturaProveedor Registrar(CEFacturaProveedor facturaproveedor)
        {
            if (ValidarProducto(facturaproveedor))
            {
                var factura = _facturaproveedorDal.Insert(facturaproveedor);
                //Calculo el digito verificador de lo que inserte
                var digVerNeg = new CNDigVer();
                digVerNeg.actualizarDVH("Facturas", facturaproveedor.Id);
                return factura;
            }
            return null;
        }

        public List<CEFacturaProveedor> Todos()
        {
            return _facturaproveedorDal.GetAll();
        }
        //public DataTable ObtenerProvedor()
        //{
        //}

        public CEFacturaProveedor TraerPorId(int idProduct)
        {
            stringBuilder.Clear();

            if (idProduct == 0) stringBuilder.Append("Por favor proporcione un valor de Id valido");

            if (stringBuilder.Length == 0)
            {
                return _facturaproveedorDal.GetByid(idProduct);
            }
            return null;
        }

        public void Eliminar(int idProduct)
        {
            stringBuilder.Clear();

            if (idProduct == 0) stringBuilder.Append("Por favor proporcione un valor de Id valido");

            if (stringBuilder.Length == 0)
            {
                _facturaproveedorDal.Delete(idProduct);
            }
        }

        public List<CEEstadoFactura> GetEstadosFactura()
        {
            return _facturaproveedorDal.GetEstadosFactura();
        }

        private bool ValidarProducto(CEFacturaProveedor facturaproveedor)
        {
            stringBuilder.Clear();

            //if (string.IsNullOrEmpty(facturaproveedor.Descripcion)) stringBuilder.Append("El campo Descripción es obligatorio");
            //if (string.IsNullOrEmpty(facturaproveedor.Marca)) stringBuilder.Append(Environment.NewLine + "El campo Marca es obligatorio");
            //if (facturaproveedor.Precio <= 0) stringBuilder.Append(Environment.NewLine + "El campo Precio es obligatorio");

            return stringBuilder.Length == 0;
        }
    }
}