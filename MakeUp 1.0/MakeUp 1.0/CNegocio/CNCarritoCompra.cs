﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDatos;
using CEntidades;

namespace CNegocio
{
    //Clase que contiene una lista y maneja el carrito de compra agregando, sacando items de productos.
    public class CNCarritoCompra
    {
        //Atributos privados de la clase que maneja todo el carrito de compra.
        private List<CEItemProducto> productos = null;

        //private Database.Usuario user = null;
        private CNUsuario user = null;

        private int importe;
        //Constructos de la clase que inicializa la lista de items y el importe 
        //total en 0 para comenzar con la transaccion online.
        public CNCarritoCompra()
        {
            productos = new List<CEItemProducto>();
            importe = 0;
        }
        //Metodo que agrega items del tipo ItemProducto a la lista de la clase.
        public void agregarProducto(CEItemProducto p)
        {
            this.productos.Add(p);
        }
        //Metodo que quita productos de la lista de la clase.
        public void quitarProducto(CEItemProducto p)
        {
            CEItemProducto prod2 = this.productos.Find(prod => prod.id == p.id);
            this.productos.Remove(prod2);
        }
        //Metodo que obtiene el importe total de los productos cuando se quiere mostrar por pantalla
        //el total. Recorre todos los items y acumula el subtotal de cada uno.
        public int obtenerImporte()
        {
            importe = 0;
            foreach (CEItemProducto item in productos)
            {
                importe += item.cantidad * item.precio;

            }

            return importe;
        }
        //Propiedad que devuelve la lista de productos para mostrar por pantalla 
        //los items que se encuentran en el carrito de compra.
        public List<CEItemProducto> obtenerProductos
        {
            get { return this.productos; }
        }
        //Propiedad que devuelve la cantidad de productos que se encuentran en la lista
        //para mostrar por pantalla a medida que se van agregando items al carrito.
        public int CantidadDeProductos
        {
            get { return this.productos.Count; }
        }
        //Propiedad que devuelve si existe el producto en la lista para validar y mostrar
        //al cliente si eligio agregar un producto que ya se encontraba en la lista.
        public bool existeProducto(CEItemProducto p)
        {

            return this.productos.Exists(prod => prod.id == p.id);

        }

    }
}