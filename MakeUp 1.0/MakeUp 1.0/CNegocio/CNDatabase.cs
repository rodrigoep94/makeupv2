﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Microsoft.SqlServer;
using CDatos;

namespace CNegocio
{
    //Clase que maneja el backup y restore para seguridad de la aplicacion.
    public class CNDatabase
    {
        private CDDatabase _databaseDal = new CDDatabase();
        //Metodo que realiza el backup de la base de datos, recibe por
        //parametro el nombre de la base de datos y un string que se pasa por
        //referencia para asignar en caso de que haya un error el valor
        //del error de base de datos.
        public bool RealizarBackup(string nombre, ref string message)
        {
            try
            {
                bool isOk = false;
                _databaseDal.Backup(nombre, message);
                isOk = true;
                return isOk;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        //Metodo que realiza el restore de la base de datos. Recibe por parametros el nombre del 
        //archivo del .bak para realizar el restore y un string que se pasa por
        //referencia para asignar en caso de que haya un error el valor
        //del error de base de datos.
        public bool RelizarRestore(string archivo, ref string message)
        {
            return _databaseDal.Restore(archivo, message);

        }

    }
}