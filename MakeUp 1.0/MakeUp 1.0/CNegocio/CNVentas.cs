﻿using CDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNegocio
{
    public class CNVentas
    {
        private CDVenta _CDVentasDal = new CDVenta();

        public DataSet ObtenerVentas(int idUsuario)
        {
            return _CDVentasDal.ObtenerDatosDeVenta(idUsuario);
        }
    }
}
