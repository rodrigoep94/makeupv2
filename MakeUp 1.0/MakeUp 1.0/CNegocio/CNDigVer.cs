﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Text;
using CDatos;
using Cnegocio;

namespace CNegocio
{
    public class CNDigVer
    {
        //Aplico el patron State para manejar las excepciones de los DV

        private CDDigVer _digVerDal = new CDDigVer();
        private CNBitacora bitacora;
        private CNegocio.State state;

        //Constructo de la clase DigVer, instancio un objeto Bitacora para insertar los sucesos en caso de que haya algun
        //problema de DVH y DVV.
        //Por default instancio el objeto state con el patron singleton para asegurarme una sola instancia del objeto.
        public CNDigVer()
        {
            bitacora = new CNBitacora();
            state = Optimo.getInstance();

        }

        //En caso de que nos encontremos con un error de DVH se llama a este metodo para cambiarle de instancia al objeto
        //e insertar en bitacora el suceso. Por parametro recibe la tabla en la que se encontro el suceso y el registro.
        private void ErrorRegistroState(string tableName, int idReg)
        {
            state = ErrorRegistro.getInstance();
            bitacora.ErrorDVHState(tableName, idReg);
            state.tabla = tableName;
            state.registro = idReg.ToString();
        }

        //En caso de que nos encontremos con un error de DVV se llama a este metodo para cambiarle de instancia al objeto
        //e insertar en bitacora el suceso. Recibe por parametro el nombre de la tabla donde se encontro el suceso de DVV.
        private void ErrorTablaState(string tableName)
        {
            state = ErrorTabla.getInstance();
            bitacora.ErrorDVVState(tableName);
            state.tabla = tableName;
        }

        //Metodo que se llama cuando se inicializa el sistema para comprobar la integridad de la base de datos.
        public State ComprobarDV()
        {
            var tableDV = _digVerDal.GetTable();
            //Recorro los items de la tabla.
            foreach (DataRow item in tableDV.Rows)
            {
                string tableName = item["tabla"].ToString();
                int DVV = Convert.ToInt32(item["dvv"]);
                //Metodo que realiza el calculo del digito verificado por tabla. 
                CalcularDV(tableName, DVV);
                //Dependiendo el tipo de la clase que tiene en este momento (Patron State) devuelvo su mensaje de estado.
                if (!state.GetType().Name.Equals("Optimo"))
                {
                    return this.state;
                }
            }

            return this.state;
        }

        //Calculo private del digito verificado, recibe por parametro el nombre de la tabla y el numero del DVV que se encuentra
        //en la tabla de Digitos verificadores.
        private void CalcularDV(string tableName, int DVV)
        {

            int resultV = 0;

            var tablaDv = _digVerDal.GetValoresDeTabla(tableName);

            List<String> lista = new List<String>();
            //Variable que concatena todos los registros en un string para realizar el calculo.
            String suma = "";
            //Recorro los items de la tabla.
            foreach (DataRow item in tablaDv.Rows)
            {
                //Recorro todas las columnas para concatenar.
                for (int i = 0; i < tablaDv.Columns.Count; i++)
                {
                    //Concateno todos los campos menos el del DV
                    if (!tablaDv.Columns[i].ColumnName.Equals("dv"))
                    {
                        //Concatenacion
                        suma += item[i].ToString();
                    }
                }
                //Convierto la varible string en un array de bytes que en este caso me devuelve el valor del ascii correspondiente
                //a cada caracter del valor de la variable. Para luego multiplicarlo por su posicion.
                Byte[] array = Encoding.ASCII.GetBytes(suma);
                int posicion = 0;
                int resultH = 0;

                foreach (byte element in array)
                {
                    //Voy obteniendo el DVH, calculo el resultado del ASCII del caracter por su posicion y lo acumulo
                    //en la variable ResultH.
                    posicion += 1;
                    resultH += ((int)element) * posicion;

                }

                //Obtengo el DVH que posee el actual registro para validarlo contra el que calulamos recien.
                int DVH = Convert.ToInt32(item["dv"]);
                //Si no es igual el numero, llamo al metodo de error de registro el cual mediante el patron state hace
                //que se cambien el tipo de la clase y se inserta el suceso en bitacora.
                if (!resultH.Equals(DVH))
                {
                    ErrorRegistroState(tableName, Convert.ToInt32(item["id"]));
                    //realizo el break y no sigo calculando digitos verificadores.
                    break;
                }
                //voy acumulando todos los valores de los registros del campo DVH para luego comparar contra el DVV.
                resultV += resultH;
                //limpio los datos de la variable suma para seguir en el foreach.
                suma = "";
            }
            //Calculo de DVV en caso de que el tipo de la clase sea Optimo.

            if (state.GetType().Name.Equals("Optimo"))
            {
                //Si no es igual el DVV calculado al de la base de datos que este caso se obtuvo por parametro llamo al metodo 
                //de error de tabla para cambia el tipo de clase al que corresponde e inserta el suceso en bitacora.
                if (!resultV.Equals(DVV))
                {
                    ErrorTablaState(tableName);

                }
            }


        }

        //Metodo que acutaliza el DVV de la tabla para cuando se realizan operaciones de insert o update.
        public void actualizarDVV(string tablename)
        {
            _digVerDal.ActualizarDVVTabla(tablename);
        }


        //Metodo que calcula el DVH del registro que se actualizo o inserto para realizar el update correspondiente en el campo DVH.
        //Y luego llama al metodo actualizarDVV para la actualizacion del DVV de la misma tabla.
        public void actualizarDVH(string tablename, int id)
        {
            string suma = "";
            var table = _digVerDal.GetValorDeTabla(tablename, id);

            if (table.Rows.Count > 0)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (!table.Columns[i].ColumnName.Equals("dv"))
                    {
                        //concateno todos los campos menos el del DVH
                        suma += table.Rows[0][i].ToString();


                    }
                }
                Byte[] array = Encoding.ASCII.GetBytes(suma);
                int posicion = 0;
                int resultH = 0;

                foreach (byte element in array)
                {
                    //Calculo del DVH
                    posicion += 1;
                    resultH += ((int)element) * posicion;

                }

                _digVerDal.UpdateDV(tablename, resultH, id);
                actualizarDVV(tablename);
            }

        }

        //Este metodo solo se usa para actualizar las tablas en la base de datos para reestablecer la integridad si se produjo algun tipo
        //de error de digito verificador.
        public void actualizarDVH(string tablename)
        {

            String suma = "";
            var table = _digVerDal.GetValoresDeTabla(tablename);
            foreach (DataRow item in table.Rows)
            {

                for (int i = 0; i < table.Columns.Count; i++)
                {

                    if (!table.Columns[i].ColumnName.Equals("dv"))
                    {

                        suma += item[i].ToString();


                    }
                }
                Byte[] array = Encoding.ASCII.GetBytes(suma);
                int posicion = 0;
                int resultH = 0;

                foreach (byte element in array)
                {
                    posicion += 1;
                    resultH += ((int)element) * posicion;

                }
                int id = Convert.ToInt32(item["id"]);

                _digVerDal.UpdateDV(tablename, resultH, id);
                suma = "";
            }


            actualizarDVV(tablename);

        }


    }
}