﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CDatos;
using CEntidades;

namespace CNegocio
{
    public class CNFacturaProveedorItem
    {
        //Instanciamos nuestra clase facturaProveedorDal para poder utilizar sus miembros
        private CDProducto _productoDal = new CDProducto();
        private CDFacturaProveedorItem _facturaproveedoritemDal = new CDFacturaProveedorItem();
        //
        //El uso de la clase StringBuilder nos ayudara a devolver los mensajes de las validaciones
        public readonly StringBuilder stringBuilder = new StringBuilder();

        public void Registrar(CEFacturaProveedorItem facturaproveedoritem)
        {
            if (ValidarProducto(facturaproveedoritem))
            {
                _facturaproveedoritemDal.Insert(facturaproveedoritem);
                _productoDal.UpdateStock(facturaproveedoritem.producto_id, facturaproveedoritem.cantidad);
//                CNegocio.CNDigVer.actualizarDVH("Facturas", facturaproveedoritem.Id);
            }
        }

        private bool ValidarProducto(CEFacturaProveedorItem facturaproveedoritem)
        {
            stringBuilder.Clear();

            //if (string.IsNullOrEmpty(facturaproveedor.Descripcion)) stringBuilder.Append("El campo Descripción es obligatorio");
            //if (string.IsNullOrEmpty(facturaproveedor.Marca)) stringBuilder.Append(Environment.NewLine + "El campo Marca es obligatorio");
            //if (facturaproveedor.Precio <= 0) stringBuilder.Append(Environment.NewLine + "El campo Precio es obligatorio");

            return stringBuilder.Length == 0;
        }


    }
}
