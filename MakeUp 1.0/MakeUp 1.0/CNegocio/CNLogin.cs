﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using CEntidades;
using CDatos;

namespace CNegocio
{
    public class CNLogin
    {
        private CDLogin _loginDal = new CDLogin();

        public List<MenuItem> SetPermisos(int userType)
        {
            var opcionesMenu = _loginDal.GetOpcionesDeMenu(userType);
            return opcionesMenu;
        }

        public bool existeUsuario(string usuario)
        {
            bool existe = false;

            var cantUsuarios = _loginDal.GetUsuario(usuario);
            existe = cantUsuarios.Equals(1);

            return existe;
        }

        public void reiniciarContador(CEUsuario user)
        {
            _loginDal.ReiniciarContador(user);
            var digVerNeg = new CNDigVer();
            digVerNeg.actualizarDVH("usuarios", user.id);
        }


        public bool AumentarContador(string usuario)
        {
            CNBitacora bitacora = new CNBitacora();
            bool estaBloqueado = false;
            int intentos = 0;
            int id = 0;

            _loginDal.GetIntentosDeUsuario(usuario, out intentos, out id);

            //Si la cantidad de intentos es 3 el usuario esta bloqueado.
            if (intentos.Equals(2))
            {
                //Inserto en la bitacora el suceso.
                bitacora.ErrorBloqueoUsuarioState(usuario);
                estaBloqueado = true;
                return estaBloqueado;
            }
            else
            {
                _loginDal.UpdateIntentosUsuario(usuario);
                //Inserto en bitacora el suceso.
                bitacora.ErrorFalloContraseñaState(usuario);
                //ACtualizo el DVH y DVV.
                var digVerNeg = new CNDigVer();
                digVerNeg.actualizarDVH("Usuarios", id);
            }
            return estaBloqueado;

        }

        public CEUsuario Autenticar(string usuario, string password)
        {
            //Concateno el usuario y contraseña y luego lo encripto ya que al dar de alta el usuario se realizo de esta misma forma
            //y asi validar contra la base de datos. El resultado de encriptacion es el mismo que se encuentra en la base de datos.
            string hash = Cnegocio.CNEncriptacion.GetMD5(string.Concat(usuario, password));
            return _loginDal.AutenticarUsuario(usuario, password, hash);

        }
    }
}