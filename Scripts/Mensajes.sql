ALTER TABLE Usuarios
ALTER COLUMN id INT NOT NULL;

ALTER TABLE Usuarios
ADD PRIMARY KEY (id);

CREATE TABLE Mensajes (id_mensaje INT IDENTITY(1,1) PRIMARY KEY,
					   msj_emisor VARCHAR(MAX),
					   msj_emisor_id INT FOREIGN KEY REFERENCES Usuarios(id),
					   msj_fecha DATETIME,
					   msj_destinatario VARCHAR(MAX),
					   msj_destinatario_id INT FOREIGN KEY REFERENCES Usuarios(id),
					   msj_texto VARCHAR(MAX));