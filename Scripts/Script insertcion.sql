USE Makeup

IF NOT EXISTS (SELECT * FROM Menu WHERE Descripcion = 'Alta de Proveedores')
BEGIN
  INSERT INTO Menu(Descripcion, Orden)
  VALUES ('Alta de Proveedores', 10)

  INSERT INTO MenuXTipoUsuario
  VALUES (2,10)
END