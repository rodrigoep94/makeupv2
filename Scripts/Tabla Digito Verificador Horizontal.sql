IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='Digito_Verificador_Horizontal' and xtype='U')
	CREATE TABLE Digito_Verificador_Horizontal (
		dvh_id INT  NOT NULL PRIMARY KEY,
		dvh_tabla VARCHAR(MAX) NOT NULL,
		dvh_dig INT  NOT NULL);
GO

IF NOT EXISTS (SELECT TOP 1 1 FROM Digito_Verificador_Horizontal WHERE dvh_tabla = 'Detalle_X_FacturaProveedor')
	INSERT INTO Digito_Verificador_Horizontal
	VALUES (1, 'Detalle_X_FacturaProveedor', 0)

IF NOT EXISTS (SELECT TOP 1 1 FROM Digito_Verificador_Horizontal WHERE dvh_tabla = 'Estados')
	INSERT INTO Digito_Verificador_Horizontal
	VALUES (2, 'Estados', 0)

IF NOT EXISTS (SELECT TOP 1 1 FROM Digito_Verificador_Horizontal WHERE dvh_tabla = 'Facturas')
	INSERT INTO Digito_Verificador_Horizontal
	VALUES (3, 'Facturas', 0)

IF NOT EXISTS (SELECT TOP 1 1 FROM Digito_Verificador_Horizontal WHERE dvh_tabla = 'Productos')
	INSERT INTO Digito_Verificador_Horizontal
	VALUES (4, 'Productos', 0)

IF NOT EXISTS (SELECT TOP 1 1 FROM Digito_Verificador_Horizontal WHERE dvh_tabla = 'Proveedores')
	INSERT INTO Digito_Verificador_Horizontal
	VALUES (5, 'Proveedores', 0)

IF NOT EXISTS (SELECT TOP 1 1 FROM Digito_Verificador_Horizontal WHERE dvh_tabla = 'Remitos')
	INSERT INTO Digito_Verificador_Horizontal
	VALUES (6, 'Remitos', 0)

IF NOT EXISTS (SELECT TOP 1 1 FROM Digito_Verificador_Horizontal WHERE dvh_tabla = 'Telefonos')
	INSERT INTO Digito_Verificador_Horizontal
	VALUES (7, 'Telefonos', 0)

IF NOT EXISTS (SELECT TOP 1 1 FROM Digito_Verificador_Horizontal WHERE dvh_tabla = 'Usuarios')
	INSERT INTO Digito_Verificador_Horizontal
	VALUES (8, 'Usuarios', 0)

